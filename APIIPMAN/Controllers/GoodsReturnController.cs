﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class GoodsReturnController : ApiController
    {

        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitGoodsReturnResponse submitGoodsReturn(string submitGoodsReturn, SubmitGoodsReturnRequest param)
        {
            SubmitGoodsReturnResponse output = new SubmitGoodsReturnResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDueDate = DateTime.ParseExact(param.dueDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_GoodsReturn.OrderByDescending(p => p.GoodsReturnId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.GoodsReturnCode.Split('-');
                    storyCode = "GRR-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "GRR-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_GoodsReturn t_GoodsReturn = new T_GoodsReturn();
                    t_GoodsReturn.GoodsReturnCode = storyCode;
                    t_GoodsReturn.GoodsReturnPostingDate = dtPostingDate;
                    t_GoodsReturn.GoodsReturnDocumentDate = dtDocumentDate;
                    t_GoodsReturn.GoodsReturnDueDate = dtDueDate;
                    t_GoodsReturn.GoodsReturnVendorCode = param.vendorCode;
                    t_GoodsReturn.GoodsReturnVendorName = param.vendorName;
                    t_GoodsReturn.GoodsReturnWhsCode = param.whsCode;
                    t_GoodsReturn.GoodsReturnWhsName = param.whsName;
                    t_GoodsReturn.GoodsReturnNotes = param.notes;
                    t_GoodsReturn.BPLId = param.branchCode;
                    t_GoodsReturn.CreatedBy = param.userId;
                    t_GoodsReturn.CreatedOn = dt;
                    t_GoodsReturn.ModifiedBy = param.userId;
                    t_GoodsReturn.ModifiedOn = dt;
                    db.T_GoodsReturn.Add(t_GoodsReturn);
                    db.SaveChanges();
                    List<GoodsReturnDetailClass> PSJDD = JsonConvert.DeserializeObject<List<GoodsReturnDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_GoodsReturnDetail t_GoodsReturnDetail = new T_GoodsReturnDetail();
                        t_GoodsReturnDetail.GoodsReturnCode = t_GoodsReturn.GoodsReturnCode;
                        t_GoodsReturnDetail.GoodsReturnDetailItemCode = datasetdetail.itemCode;
                        t_GoodsReturnDetail.GoodsReturnDetailItemName = datasetdetail.itemName;
                        t_GoodsReturnDetail.GoodsReturnDetailItemBarcode = datasetdetail.itemBarcode;
                        t_GoodsReturnDetail.GoodsReturnDetailItemQty = datasetdetail.itemQty;
                        t_GoodsReturnDetail.GoodsReturnDetailItemPrice = datasetdetail.itemPrice;
                        t_GoodsReturnDetail.GoodsReturnDetailItemType = datasetdetail.itemType;
                        t_GoodsReturnDetail.GoodsReturnDetailItemVendorCode = datasetdetail.vendorCode;
                        t_GoodsReturnDetail.GoodsReturnDetailItemVendorName = datasetdetail.vendorName;
                        t_GoodsReturnDetail.GoodsReturnDetailItemWhsCode = datasetdetail.whsCode;
                        t_GoodsReturnDetail.GoodsReturnDetailItemWhsName = datasetdetail.whsName;
                        t_GoodsReturnDetail.GoodsReturnDetailItemUoMcode = datasetdetail.itemUoMCode;
                        t_GoodsReturnDetail.GRPODocNum = datasetdetail.docNum;
                        t_GoodsReturnDetail.GRPODocEntry = datasetdetail.docEntry;
                        t_GoodsReturnDetail.GRPOLineNum = datasetdetail.lineNum;
                        t_GoodsReturnDetail.CreatedBy = param.userId;
                        t_GoodsReturnDetail.CreatedOn = dt;
                        t_GoodsReturnDetail.ModifiedBy = param.userId;
                        t_GoodsReturnDetail.ModifiedOn = dt;
                        db.T_GoodsReturnDetail.Add(t_GoodsReturnDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_GoodsReturn.GoodsReturnCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.goodsReturnId = t_GoodsReturn.GoodsReturnId;
                    output.goodsReturnCode = t_GoodsReturn.GoodsReturnCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.goodsReturnId = 0;
                output.goodsReturnCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetGoodsReturnResponse getGoodsReturn(string getGoodsReturn, GetGoodsReturnRequest param)
        {
            GetGoodsReturnResponse output = new GetGoodsReturnResponse();
            try
            {
                output.headers = new List<GetGoodsReturnClass>();
                output.details = new List<GetGoodsReturnDetailClass>();
                var getGoodsReturnHeader = (from th in db.T_GoodsReturn.Where(p => p.GoodsReturnWhsCode == param.whsCode && p.SyncDate == null)
                                        from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                        from em in db.T_Log.Where(p => p.LogDocId == th.GoodsReturnId && p.LogDocCode == th.GoodsReturnCode
                                        && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                            group new { th, td } by new
                                     {
                                         th.GoodsReturnCode,
                                         th.GoodsReturnPostingDate,
                                         th.GoodsReturnDocumentDate,
                                         th.GoodsReturnDueDate,
                                         th.GoodsReturnVendorCode,
                                         th.GoodsReturnVendorName,
                                         td.UserCode,
                                         td.UserName,
                                         th.GoodsReturnNotes,
                                         th.GoodsReturnId,
                                         th.CreatedOn,
                                         em.LogMessage
                                     }
                                             into g
                                     select new
                                     {
                                         g.Key.GoodsReturnCode,
                                         g.Key.GoodsReturnPostingDate,
                                         g.Key.GoodsReturnDocumentDate,
                                         g.Key.GoodsReturnDueDate,
                                         g.Key.GoodsReturnVendorCode,
                                         g.Key.GoodsReturnVendorName,
                                         g.Key.UserCode,
                                         g.Key.UserName,
                                         g.Key.GoodsReturnNotes,
                                         g.Key.GoodsReturnId,
                                         g.Key.CreatedOn,
                                         g.Key.LogMessage
                                     }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getGoodsReturnHeader.Count; i++)
                {
                    string code = getGoodsReturnHeader[i].GoodsReturnCode;
                    GetGoodsReturnClass getGoodsReturnClass = new GetGoodsReturnClass();
                    getGoodsReturnClass.goodsReturnCode = getGoodsReturnHeader[i].GoodsReturnCode;
                    getGoodsReturnClass.goodsReturnPostingDate = getGoodsReturnHeader[i].GoodsReturnPostingDate?.ToString("dd-MMM-yyyy");
                    getGoodsReturnClass.goodsReturnDocumentDate = getGoodsReturnHeader[i].GoodsReturnDocumentDate?.ToString("dd-MMM-yyyy");
                    getGoodsReturnClass.goodsReturnDueDate = getGoodsReturnHeader[i].GoodsReturnDueDate?.ToString("dd-MMM-yyyy");
                    getGoodsReturnClass.goodsReturnVendorCode = getGoodsReturnHeader[i].GoodsReturnVendorCode;
                    getGoodsReturnClass.goodsReturnVendorName = getGoodsReturnHeader[i].GoodsReturnVendorName;
                    getGoodsReturnClass.goodsReturnUserCode = getGoodsReturnHeader[i].UserCode;
                    getGoodsReturnClass.goodsReturnUserName = getGoodsReturnHeader[i].UserName;
                    getGoodsReturnClass.goodsReturnNotes = getGoodsReturnHeader[i].GoodsReturnNotes;
                    getGoodsReturnClass.errorMessage = getGoodsReturnHeader[i].LogMessage;
                    output.headers.Add(getGoodsReturnClass);
                    var getGoodsReturnDetails = db.T_GoodsReturnDetail.Where(p => p.GoodsReturnCode == code).ToList();
                    for (int j = 0; j < getGoodsReturnDetails.Count; j++)
                    {
                        GetGoodsReturnDetailClass getGoodsReturnDetailClass = new GetGoodsReturnDetailClass();
                        getGoodsReturnDetailClass.goodsReturnDetailId = getGoodsReturnDetails[j].GoodsReturnDetailId;
                        getGoodsReturnDetailClass.goodsReturnDetailitemCode = getGoodsReturnDetails[j].GoodsReturnDetailItemCode;
                        getGoodsReturnDetailClass.goodsReturnDetailitemName = getGoodsReturnDetails[j].GoodsReturnDetailItemName;
                        getGoodsReturnDetailClass.goodsReturnDetailitemBarcode = getGoodsReturnDetails[j].GoodsReturnDetailItemBarcode;
                        getGoodsReturnDetailClass.goodsReturnDetailitemType = getGoodsReturnDetails[j].GoodsReturnDetailItemType;
                        getGoodsReturnDetailClass.goodsReturnDetailitemQty = getGoodsReturnDetails[j].GoodsReturnDetailItemQty;
                        getGoodsReturnDetailClass.goodsReturnDetailitemPrice = getGoodsReturnDetails[j].GoodsReturnDetailItemPrice;
                        getGoodsReturnDetailClass.goodsReturnCode = getGoodsReturnDetails[j].GoodsReturnCode;
                        getGoodsReturnDetailClass.GRPOCode = getGoodsReturnDetails[j].GRPODocNum;
                        getGoodsReturnDetailClass.goodsReturnDetailitemOpenQty = 0;
                        getGoodsReturnDetailClass.goodsReturnDetailItemUoMCode = getGoodsReturnDetails[j].GoodsReturnDetailItemUoMcode;
                        getGoodsReturnDetailClass.goodsReturnDetailVendorCode = getGoodsReturnDetails[j].GoodsReturnDetailItemVendorCode;
                        getGoodsReturnDetailClass.goodsReturnDetailVendorName = getGoodsReturnDetails[j].GoodsReturnDetailItemVendorName;
                        getGoodsReturnDetailClass.goodsReturnDetailWhsCode = getGoodsReturnDetails[j].GoodsReturnDetailItemWhsCode;
                        getGoodsReturnDetailClass.goodsReturnDetailWhsName = getGoodsReturnDetails[j].GoodsReturnDetailItemWhsName;
                        getGoodsReturnDetailClass.docEntry = getGoodsReturnDetails[j].GRPODocEntry;
                        getGoodsReturnDetailClass.docNum = getGoodsReturnDetails[j].GRPODocNum;
                        output.details.Add(getGoodsReturnDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
