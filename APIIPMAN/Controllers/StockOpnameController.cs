﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class StockOpnameController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitStockOpnameResponse submitStockOpname(string submitStockOpname, SubmitStockOpnameRequest param)
        {
            SubmitStockOpnameResponse output = new SubmitStockOpnameResponse();
            try
            {
                var dtCountingDate = DateTime.ParseExact(param.countingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_StockOpname t_StockOpname = new T_StockOpname();
                    t_StockOpname.StockOpnameCountingDate = dtCountingDate;
                    t_StockOpname.StockOpnamePostingDate = dtPostingDate;
                    t_StockOpname.StockOpnameWhsCode = param.whsCode;
                    t_StockOpname.StockOpnameWhsName = param.whsName;
                    t_StockOpname.StockOpnameNotes = param.notes;
                    t_StockOpname.DocNum = param.docNum;
                    t_StockOpname.DocEntry = param.docEntry;
                    t_StockOpname.BPLId = param.branchCode;
                    t_StockOpname.CreatedBy = param.userId;
                    t_StockOpname.CreatedOn = dt;
                    t_StockOpname.ModifiedBy = param.userId;
                    t_StockOpname.ModifiedOn = dt;
                    db.T_StockOpname.Add(t_StockOpname);
                    db.SaveChanges();
                    List<StockOpnameDetailClass> PSJDD = JsonConvert.DeserializeObject<List<StockOpnameDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_StockOpnameDetail t_StockOpnameDetail = new T_StockOpnameDetail();
                        t_StockOpnameDetail.StockOpnameDetailItemCode = datasetdetail.itemCode;
                        t_StockOpnameDetail.StockOpnameDetailItemName = datasetdetail.itemName;
                        t_StockOpnameDetail.StockOpnameDetailItemBarcode = datasetdetail.itemBarcode;
                        t_StockOpnameDetail.StockOpnameDetailItemQty = datasetdetail.itemQty;
                        t_StockOpnameDetail.StockOpnameDetailItemItemType = datasetdetail.itemType;
                        t_StockOpnameDetail.StockOpnameDetailItemWhsCode = datasetdetail.whsCode;
                        t_StockOpnameDetail.StockOpnameDetailItemWhsName = datasetdetail.whsName;
                        t_StockOpnameDetail.LineNum = datasetdetail.lineNum;
                        t_StockOpnameDetail.DocNum = t_StockOpname.DocNum;
                        t_StockOpnameDetail.DocEntry = t_StockOpname.DocEntry;
                        t_StockOpnameDetail.CreatedBy = param.userId;
                        t_StockOpnameDetail.CreatedOn = dt;
                        t_StockOpnameDetail.ModifiedBy = param.userId;
                        t_StockOpnameDetail.ModifiedOn = dt;
                        db.T_StockOpnameDetail.Add(t_StockOpnameDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_StockOpname.DocNum;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }

        [HttpPost]
        public GetStockOpnameResponse getStockOpname(string getStockOpname, GetStockOpnameRequest param)
        {
            GetStockOpnameResponse output = new GetStockOpnameResponse();
            output.headers = new List<GetStockOpnameClass>();
            try
            {
                var getAllStockOpname = (from th in db.T_StockOpname.Where(p => p.SyncDate == null)
                                           from em in db.T_Log.Where(p => p.LogDocId == th.StockOpnameId && p.LogDocCode == th.DocNum
                                           && p.LogStatus != "Cancel" && p.LogStatus != "Success")
                                         group new { th, em } by new
                                           {
                                               th.StockOpnameId,
                                               th.StockOpnameCode,
                                               th.DocNum,
                                               th.DocEntry,
                                               em.LogId,
                                               em.LogMessage
                                           }
                                             into g
                                           select new
                                           {
                                               g.Key.StockOpnameId,
                                               g.Key.StockOpnameCode,
                                               g.Key.DocNum,
                                               g.Key.DocEntry,
                                               g.Key.LogId,
                                               g.Key.LogMessage
                                           }).OrderByDescending(p => p.StockOpnameId).ToList();
                for (int i = 0; i < getAllStockOpname.Count; i++)
                {
                    GetStockOpnameClass getStockOpnameClass = new GetStockOpnameClass();
                    getStockOpnameClass.stockOpnameId = getAllStockOpname[i].StockOpnameId;
                    getStockOpnameClass.stockOpnameCode = getAllStockOpname[i].StockOpnameCode;
                    getStockOpnameClass.docNum = getAllStockOpname[i].DocNum;
                    getStockOpnameClass.docEntry = getAllStockOpname[i].DocEntry;
                    getStockOpnameClass.logId = getAllStockOpname[i].LogId;
                    getStockOpnameClass.logMessage = getAllStockOpname[i].LogMessage;
                    output.headers.Add(getStockOpnameClass);
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }

        [HttpPost]
        public GetStockOpnameDetailByIdResponse getStockOpnameDetailById(string getStockOpnameDetailById, GetStockOpnameDetailByIdRequest param)
        {
            GetStockOpnameDetailByIdResponse output = new GetStockOpnameDetailByIdResponse();
            output.details = new List<GetStockOpnameDetailClass>();
            try
            {
                var getStockOpnameDetail = db.T_StockOpnameDetail.Where(p => p.DocNum == param.docNum).ToList();
                for (int i = 0; i < getStockOpnameDetail.Count; i++)
                {
                    GetStockOpnameDetailClass getStockOpnameDetailClass = new GetStockOpnameDetailClass();
                    getStockOpnameDetailClass.stockOpnameDetailId = getStockOpnameDetail[i].StockOpnameDetailId;
                    getStockOpnameDetailClass.stockOpnameDetailItemCode = getStockOpnameDetail[i].StockOpnameDetailItemCode;
                    getStockOpnameDetailClass.stockOpnameDetailItemName = getStockOpnameDetail[i].StockOpnameDetailItemName;
                    getStockOpnameDetailClass.stockOpnameDetailItemBarcode = getStockOpnameDetail[i].StockOpnameDetailItemBarcode;
                    getStockOpnameDetailClass.stockOpnameDetailWhsCode = getStockOpnameDetail[i].StockOpnameDetailItemWhsCode;
                    getStockOpnameDetailClass.stockOpnameDetailWhsName = getStockOpnameDetail[i].StockOpnameDetailItemWhsName;
                    getStockOpnameDetailClass.stockOpnameDetailCountQty = getStockOpnameDetail[i].StockOpnameDetailItemQty;
                    getStockOpnameDetailClass.stockOpnameDetailItemType = getStockOpnameDetail[i].StockOpnameDetailItemItemType;
                    getStockOpnameDetailClass.docEntry = getStockOpnameDetail[i].DocEntry;
                    getStockOpnameDetailClass.lineNum = getStockOpnameDetail[i].LineNum;
                    output.details.Add(getStockOpnameDetailClass);
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }
    }
}
