﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class InventoryTransferController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitInventoryTransferResponse submitInventoryTransfer(string submitInventoryTransfer, SubmitInventoryTransferRequest param)
        {
            SubmitInventoryTransferResponse output = new SubmitInventoryTransferResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                string status = "";
                if (param.type == "Out")
                {
                    storyCode = "ITO-";
                    status = "Pending";
                }
                else
                {
                    storyCode = "ITI-";
                    status = "Success";
                }
                var checkLastStory = db.T_InventoryTransfer.OrderByDescending(p => p.InventoryTransferId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.InventoryTransferCode.Split('-');
                    storyCode = storyCode + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = storyCode + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_InventoryTransfer t_InventoryTransfer = new T_InventoryTransfer();
                    t_InventoryTransfer.InventoryTransferCode = storyCode;
                    t_InventoryTransfer.InventoryTransferPostingDate = dtPostingDate;
                    t_InventoryTransfer.InventoryTransferDocumentDate = dtDocumentDate;
                    t_InventoryTransfer.InventoryTransferFromWhsCode = param.fromCode;
                    t_InventoryTransfer.InventoryTransferFromWhsName = param.fromName;
                    t_InventoryTransfer.InventoryTransferToWhsCode = param.toCode;
                    t_InventoryTransfer.InventoryTransferToWhsName = param.toName;
                    t_InventoryTransfer.InventoryTransferStatus = status;
                    t_InventoryTransfer.InventoryTransferNotes = param.notes;
                    t_InventoryTransfer.BPLId = param.branchCode;
                    t_InventoryTransfer.CreatedBy = param.userId;
                    t_InventoryTransfer.CreatedOn = dt;
                    t_InventoryTransfer.ModifiedBy = param.userId;
                    t_InventoryTransfer.ModifiedOn = dt;
                    db.T_InventoryTransfer.Add(t_InventoryTransfer);
                    db.SaveChanges();
                    List<InventoryTransferDetailClass> PSJDD = JsonConvert.DeserializeObject<List<InventoryTransferDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_InventoryTransferDetail t_InventoryTransferDetail = new T_InventoryTransferDetail();
                        t_InventoryTransferDetail.InventoryTransferCode = t_InventoryTransfer.InventoryTransferCode;
                        t_InventoryTransferDetail.InventoryTransferDetailItemCode = datasetdetail.itemCode;
                        t_InventoryTransferDetail.InventoryTransferDetailItemName = datasetdetail.itemName;
                        t_InventoryTransferDetail.InventoryTransferDetailItemBarcode = datasetdetail.itemBarcode;
                        t_InventoryTransferDetail.InventoryTransferDetailItemQty = datasetdetail.itemQty;
                        t_InventoryTransferDetail.InventoryTransferDetailItemType = datasetdetail.itemType;
                        t_InventoryTransferDetail.InventoryTransferDetailItemUoMCode = datasetdetail.itemUomCode;
                        t_InventoryTransferDetail.CreatedBy = dt;
                        t_InventoryTransferDetail.CreatedOn = param.userId;
                        t_InventoryTransferDetail.ModifiedBy = dt;
                        t_InventoryTransferDetail.ModifiedOn = param.userId;
                        db.T_InventoryTransferDetail.Add(t_InventoryTransferDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_InventoryTransfer.InventoryTransferCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.inventoryTransferId = t_InventoryTransfer.InventoryTransferId;
                    output.inventoryTransferCode = t_InventoryTransfer.InventoryTransferCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.inventoryTransferId = 0;
                output.inventoryTransferCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetInventoryTransferResponse getInventoryTransfer(string getInventoryTransfer, GetInventoryTransferRequest param)
        {
            GetInventoryTransferResponse output = new GetInventoryTransferResponse();
            try
            {
                output.headers = new List<GetInventoryTransferClass>();
                output.details = new List<GetInventoryTransferDetailClass>();
                var getInventoryTransferHeader = (from th in db.T_InventoryTransfer.Where(p => p.SyncDate == null)
                                           from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                           from em in db.T_Log.Where(p => p.LogDocId == th.InventoryTransferId && p.LogDocCode == th.InventoryTransferCode
                                           && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                                  group new { th, td } by new
                                           {
                                               th.InventoryTransferCode,
                                               th.InventoryTransferPostingDate,
                                               th.InventoryTransferDocumentDate,
                                               th.InventoryTransferFromWhsCode,
                                               th.InventoryTransferFromWhsName,
                                               th.InventoryTransferToWhsCode,
                                               th.InventoryTransferToWhsName,
                                               th.InventoryTransferNotes,
                                               th.CreatedOn,
                                               em.LogMessage
                                           }
                                             into g
                                           select new
                                           {
                                               g.Key.InventoryTransferCode,
                                               g.Key.InventoryTransferPostingDate,
                                               g.Key.InventoryTransferDocumentDate,
                                               g.Key.InventoryTransferFromWhsCode,
                                               g.Key.InventoryTransferFromWhsName,
                                               g.Key.InventoryTransferToWhsCode,
                                               g.Key.InventoryTransferToWhsName,
                                               g.Key.InventoryTransferNotes,
                                               g.Key.CreatedOn,
                                               g.Key.LogMessage
                                           }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getInventoryTransferHeader.Count; i++)
                {
                    string code = getInventoryTransferHeader[i].InventoryTransferCode;
                    GetInventoryTransferClass getInventoryTransferClass = new GetInventoryTransferClass();
                    getInventoryTransferClass.inventoryTransferCode = getInventoryTransferHeader[i].InventoryTransferCode;
                    getInventoryTransferClass.inventoryTransferPostingDate = getInventoryTransferHeader[i].InventoryTransferPostingDate?.ToString("dd-MMM-yyyy");
                    getInventoryTransferClass.inventoryTransferDocumentDate = getInventoryTransferHeader[i].InventoryTransferDocumentDate?.ToString("dd-MMM-yyyy");
                    getInventoryTransferClass.inventoryTransferFromCode = getInventoryTransferHeader[i].InventoryTransferFromWhsCode;
                    getInventoryTransferClass.inventoryTransferFromName = getInventoryTransferHeader[i].InventoryTransferFromWhsName;
                    getInventoryTransferClass.inventoryTransferToCode = getInventoryTransferHeader[i].InventoryTransferToWhsCode;
                    getInventoryTransferClass.inventoryTransferToName = getInventoryTransferHeader[i].InventoryTransferToWhsName;
                    getInventoryTransferClass.inventoryTransferNotes = getInventoryTransferHeader[i].InventoryTransferNotes;
                    getInventoryTransferClass.errorMessage = getInventoryTransferHeader[i].LogMessage;
                    output.headers.Add(getInventoryTransferClass);
                    var getInventoryTransferDetails = db.T_InventoryTransferDetail.Where(p => p.InventoryTransferCode == code).ToList();
                    for (int j = 0; j < getInventoryTransferDetails.Count; j++)
                    {
                        GetInventoryTransferDetailClass getInventoryTransferDetailClass = new GetInventoryTransferDetailClass();
                        getInventoryTransferDetailClass.inventoryTransferDetailId = getInventoryTransferDetails[j].InventoryTransferDetailId;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemCode = getInventoryTransferDetails[j].InventoryTransferDetailItemCode;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemName = getInventoryTransferDetails[j].InventoryTransferDetailItemName;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemBarcode = getInventoryTransferDetails[j].InventoryTransferDetailItemBarcode;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemType = getInventoryTransferDetails[j].InventoryTransferDetailItemType;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemQty = getInventoryTransferDetails[j].InventoryTransferDetailItemQty;
                        getInventoryTransferDetailClass.inventoryTransferDetailitemUomCode = getInventoryTransferDetails[j].InventoryTransferDetailItemUoMCode;
                        getInventoryTransferDetailClass.inventoryTransferCode = code;
                        output.details.Add(getInventoryTransferDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
