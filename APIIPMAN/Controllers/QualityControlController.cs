﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class QualityControlController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitQualityControlResponse submitQualityControl(string submitQualityControl, SubmitQualityControlRequest param)
        {
            SubmitQualityControlResponse output = new SubmitQualityControlResponse();
            try
            {
                var dtDate = DateTime.ParseExact(param.date, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_QualityControl.OrderByDescending(p => p.QualityControlId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.QualityControlCode.Split('-');
                    storyCode = "QUC-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "QUC-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_QualityControl t_QualityControl = new T_QualityControl();
                    t_QualityControl.QualityControlCode = storyCode;
                    t_QualityControl.QualityControlDate = dtDate;
                    t_QualityControl.QualityControlDocTypeName = param.type;
                    t_QualityControl.QualityControlNotes = param.notes;
                    t_QualityControl.QualityControlPhotoHeader = param.headerImage;
                    t_QualityControl.QualityControlShift = param.shift;
                    t_QualityControl.QualityControlMachine = param.machine;
                    t_QualityControl.QualityControlTime = param.time;
                    t_QualityControl.CreatedBy = param.userId;
                    t_QualityControl.CreatedOn = dt;
                    t_QualityControl.ModifiedBy = param.userId;
                    t_QualityControl.ModifiedOn = dt;
                    db.T_QualityControl.Add(t_QualityControl);
                    db.SaveChanges();
                    List<QualityControlDetailClass> PSJDD = JsonConvert.DeserializeObject<List<QualityControlDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_QualityControlDetail t_QualityControlDetail = new T_QualityControlDetail();
                        t_QualityControlDetail.QualityControlCode = t_QualityControl.QualityControlCode;
                        t_QualityControlDetail.QualityControlDetailItemCode = datasetdetail.itemCode;
                        t_QualityControlDetail.QualityControlDetailItemName = datasetdetail.itemName;
                        t_QualityControlDetail.QualityControlDetailItemUoMCode = datasetdetail.uomCode;
                        t_QualityControlDetail.QualityControlDetailItemQty = datasetdetail.qty;
                        t_QualityControlDetail.QualityControlDetailItemQtyQc = datasetdetail.qtyQc;
                        t_QualityControlDetail.QualityControlDetailItemStatusName = datasetdetail.itemStatus;
                        t_QualityControlDetail.DocNum = datasetdetail.docNum;
                        t_QualityControlDetail.DocEntry = datasetdetail.docEntry;
                        t_QualityControlDetail.LineNum = datasetdetail.lineNum;
                        db.T_QualityControlDetail.Add(t_QualityControlDetail);
                        db.SaveChanges();
                    }
                    List<QualityControlDetailImageClass> PSJDDD = JsonConvert.DeserializeObject<List<QualityControlDetailImageClass>>(param.images);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_QualityControlImage t_QualityControlImage = new T_QualityControlImage();
                        t_QualityControlImage.QualityControlDetailImage = datasetdetail.image;
                        t_QualityControlImage.QualityControlDetailStatus = datasetdetail.status;
                        t_QualityControlImage.QualityControlDetailQty = datasetdetail.qty;
                        t_QualityControlImage.QualityControlHeader = t_QualityControl.QualityControlCode;
                        t_QualityControlImage.QualityControlDetailItemCode = datasetdetail.itemCode;
                        db.T_QualityControlImage.Add(t_QualityControlImage);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.qcId = t_QualityControl.QualityControlId;
                    output.qcCode = t_QualityControl.QualityControlCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.qcId = 0;
                output.qcCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetQCDocEntryResponse getQC(string getQC, GetQCDocEntryRequest param)
        {
            GetQCDocEntryResponse output = new GetQCDocEntryResponse();
            try
            {
                var getQualityControl = (from th in db.T_QualityControl
                                            from td in db.T_QualityControlDetail.Where(p => p.QualityControlCode == th.QualityControlCode)
                                            group new { th, td } by new
                                            {
                                                td.DocEntry,
                                                td.DocNum,
                                                td.QualityControlCode
                                            }
                                             into g
                                            select new
                                            {
                                                g.Key.DocEntry,
                                                g.Key.DocNum,
                                                g.Key.QualityControlCode
                                            }).OrderByDescending(p => p.QualityControlCode).ToList();
                string strDocEntry = "";
                for (int i = 0; i < getQualityControl.Count; i++)
                {
                    if (i == 0)
                    {
                        strDocEntry = getQualityControl[i].DocEntry.ToString();
                    }
                    else
                    {
                        strDocEntry = strDocEntry + "#" + getQualityControl[i].DocEntry.ToString();
                    }
                }
                output.docEntry = strDocEntry;
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
