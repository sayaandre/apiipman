﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class DeliveryOrderController : ApiController
    {

        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitDeliveryOrderResponse submitDeliveryOrder(string submitDeliveryOrder, SubmitDeliveryOrderRequest param)
        {
            SubmitDeliveryOrderResponse output = new SubmitDeliveryOrderResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDeliveryDate = DateTime.ParseExact(param.deliveryDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_DeliveryOrder.OrderByDescending(p => p.DeliveryOrderId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.DeliveryOrderCode.Split('-');
                    storyCode = "DOR-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "DOR-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_DeliveryOrder t_DeliveryOrder = new T_DeliveryOrder();
                    t_DeliveryOrder.DeliveryOrderCode = storyCode;
                    t_DeliveryOrder.DeliveryOrderPostingDate = dtPostingDate;
                    t_DeliveryOrder.DeliveryOrderDocumentDate = dtDocumentDate;
                    t_DeliveryOrder.DeliveryOrderDeliveryDate = dtDeliveryDate;
                    t_DeliveryOrder.DeliveryOrderCustomerCode = param.customerCode;
                    t_DeliveryOrder.DeliveryOrderCustomerName = param.customerName;
                    t_DeliveryOrder.DeliveryOrderWhsCode = param.whsCode;
                    t_DeliveryOrder.DeliveryOrderWhsName = param.whsName;
                    t_DeliveryOrder.DeliveryOrderNotes = param.notes;
                    t_DeliveryOrder.BPLId = param.branchCode;
                    t_DeliveryOrder.CreatedBy = param.userId;
                    t_DeliveryOrder.CreatedOn = dt;
                    t_DeliveryOrder.ModifiedBy = param.userId;
                    t_DeliveryOrder.ModifiedOn = dt;
                    db.T_DeliveryOrder.Add(t_DeliveryOrder);
                    db.SaveChanges();
                    List<DeliveryOrderDetailClass> PSJDD = JsonConvert.DeserializeObject<List<DeliveryOrderDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_DeliveryOrderDetail t_DeliveryOrderDetail = new T_DeliveryOrderDetail();
                        t_DeliveryOrderDetail.DeliveryOrderCode = t_DeliveryOrder.DeliveryOrderCode;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemCode = datasetdetail.itemCode;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemName = datasetdetail.itemName;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemBarcode = datasetdetail.itemBarcode;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemQty = datasetdetail.itemQty;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemType = datasetdetail.itemType;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemCustomerCode = datasetdetail.customerCode;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemCustomerName = datasetdetail.customerName;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemWhsCode = datasetdetail.whsCode;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemWhsName = datasetdetail.whsName;
                        t_DeliveryOrderDetail.DeliveryOrderDetailItemUoMCode = datasetdetail.itemUoMCode;
                        t_DeliveryOrderDetail.SODocNum = datasetdetail.docNum;
                        t_DeliveryOrderDetail.SODocEntry = datasetdetail.docEntry;
                        t_DeliveryOrderDetail.SOLineNum = datasetdetail.lineNum;
                        t_DeliveryOrderDetail.CreatedBy = param.userId;
                        t_DeliveryOrderDetail.CreatedOn = dt;
                        t_DeliveryOrderDetail.ModifiedBy = param.userId;
                        t_DeliveryOrderDetail.ModifiedOn = dt;
                        db.T_DeliveryOrderDetail.Add(t_DeliveryOrderDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_DeliveryOrder.DeliveryOrderCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.deliveryOrderId = t_DeliveryOrder.DeliveryOrderId;
                    output.deliveryOrderCode = t_DeliveryOrder.DeliveryOrderCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.deliveryOrderId = 0;
                output.deliveryOrderCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetDeliveryOrderResponse getDeliveryOder(string getDeliveryOder, GetDeliveryOrderRequest param)
        {
            GetDeliveryOrderResponse output = new GetDeliveryOrderResponse();
            try
            {
                output.headers = new List<GetDeliveryOrderClass>();
                output.details = new List<GetDeliveryOrderDetailClass>();
                var getDeliveryOrderHeader = (from th in db.T_DeliveryOrder.Where(p => p.DeliveryOrderWhsCode == param.whsCode && p.SyncDate == null)
                                     from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                     from em in db.T_Log.Where(p => p.LogDocId == th.DeliveryOrderId && p.LogDocCode == th.DeliveryOrderCode 
                                     && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                              group new { th, td } by new
                                     {
                                         th.DeliveryOrderCode,
                                         th.DeliveryOrderPostingDate,
                                         th.DeliveryOrderDocumentDate,
                                         th.DeliveryOrderDeliveryDate,
                                         th.DeliveryOrderCustomerCode,
                                         th.DeliveryOrderCustomerName,
                                         td.UserCode,
                                         td.UserName,
                                         th.DeliveryOrderNotes,
                                         th.CreatedOn,
                                         em.LogMessage
                                     }
                                             into g
                                     select new
                                     {
                                         g.Key.DeliveryOrderCode,
                                         g.Key.DeliveryOrderPostingDate,
                                         g.Key.DeliveryOrderDocumentDate,
                                         g.Key.DeliveryOrderDeliveryDate,
                                         g.Key.DeliveryOrderCustomerCode,
                                         g.Key.DeliveryOrderCustomerName,
                                         g.Key.UserCode,
                                         g.Key.UserName,
                                         g.Key.DeliveryOrderNotes,
                                         g.Key.CreatedOn,
                                         g.Key.LogMessage
                                     }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getDeliveryOrderHeader.Count; i++)
                {
                    string code = getDeliveryOrderHeader[i].DeliveryOrderCode;
                    GetDeliveryOrderClass getDeliveryOrderClass = new GetDeliveryOrderClass();
                    getDeliveryOrderClass.DeliveryOrderCode = getDeliveryOrderHeader[i].DeliveryOrderCode;
                    getDeliveryOrderClass.DeliveryOrderPostingDate = getDeliveryOrderHeader[i].DeliveryOrderPostingDate?.ToString("dd-MMM-yyyy");
                    getDeliveryOrderClass.DeliveryOrderDocumentDate = getDeliveryOrderHeader[i].DeliveryOrderDocumentDate?.ToString("dd-MMM-yyyy");
                    getDeliveryOrderClass.DeliveryOrderDeliveryDate = getDeliveryOrderHeader[i].DeliveryOrderDeliveryDate?.ToString("dd-MMM-yyyy");
                    getDeliveryOrderClass.DeliveryOrderCustomerCode = getDeliveryOrderHeader[i].DeliveryOrderCustomerCode;
                    getDeliveryOrderClass.DeliveryOrderCustomerName = getDeliveryOrderHeader[i].DeliveryOrderCustomerName;
                    getDeliveryOrderClass.DeliveryOrderUserCode = getDeliveryOrderHeader[i].UserCode;
                    getDeliveryOrderClass.DeliveryOrderUserName = getDeliveryOrderHeader[i].UserName;
                    getDeliveryOrderClass.DeliveryOrderNotes = getDeliveryOrderHeader[i].DeliveryOrderNotes;
                    getDeliveryOrderClass.errorMessage = getDeliveryOrderHeader[i].LogMessage;
                    output.headers.Add(getDeliveryOrderClass);
                    var getDeliveryOrderDetails = db.T_DeliveryOrderDetail.Where(p => p.DeliveryOrderCode == code).ToList();
                    for (int j = 0; j < getDeliveryOrderDetails.Count; j++)
                    {
                        GetDeliveryOrderDetailClass getDeliveryOrderDetailClass = new GetDeliveryOrderDetailClass();
                        getDeliveryOrderDetailClass.DeliveryOrderDetailId = getDeliveryOrderDetails[j].DeliveryOrderDetailId;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemCode = getDeliveryOrderDetails[j].DeliveryOrderDetailItemCode;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemName = getDeliveryOrderDetails[j].DeliveryOrderDetailItemName;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemBarcode = getDeliveryOrderDetails[j].DeliveryOrderDetailItemBarcode;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemType = getDeliveryOrderDetails[j].DeliveryOrderDetailItemType;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemQty = getDeliveryOrderDetails[j].DeliveryOrderDetailItemQty;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemPrice = 0;
                        getDeliveryOrderDetailClass.DeliveryOrderCode = getDeliveryOrderDetails[j].DeliveryOrderCode;
                        getDeliveryOrderDetailClass.SOCode = getDeliveryOrderDetails[j].SODocNum;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailitemOpenQty = 0;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailItemUoMCode = getDeliveryOrderDetails[j].DeliveryOrderDetailItemUoMCode;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailCustomerCode = getDeliveryOrderDetails[j].DeliveryOrderDetailItemCustomerCode;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailCustomerName = getDeliveryOrderDetails[j].DeliveryOrderDetailItemCustomerName;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailWhsCode = getDeliveryOrderDetails[j].DeliveryOrderDetailItemWhsCode;
                        getDeliveryOrderDetailClass.DeliveryOrderDetailWhsName = getDeliveryOrderDetails[j].DeliveryOrderDetailItemWhsName;
                        getDeliveryOrderDetailClass.docEntry = getDeliveryOrderDetails[j].SODocEntry;
                        getDeliveryOrderDetailClass.docNum = getDeliveryOrderDetails[j].SODocNum;
                        output.details.Add(getDeliveryOrderDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
