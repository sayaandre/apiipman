﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class SalesReturnController : ApiController
    {

        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitSalesReturnResponse submitSalesReturn(string submitSalesReturn, SubmitSalesReturnRequest param)
        {
            SubmitSalesReturnResponse output = new SubmitSalesReturnResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDeliveryDate = DateTime.ParseExact(param.deliveryDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_SalesReturn.OrderByDescending(p => p.SalesReturnId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.SalesReturnCode.Split('-');
                    storyCode = "SRR-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "SRR-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_SalesReturn t_SalesReturn = new T_SalesReturn();
                    t_SalesReturn.SalesReturnCode = storyCode;
                    t_SalesReturn.SalesReturnPostingDate = dtPostingDate;
                    t_SalesReturn.SalesReturnDocumentDate = dtDocumentDate;
                    t_SalesReturn.SalesReturnDueDate = dtDeliveryDate;
                    t_SalesReturn.SalesReturnCustomerCode = param.customerCode;
                    t_SalesReturn.SalesReturnCustomerName = param.customerName;
                    t_SalesReturn.SalesReturnWhsCode = param.whsCode;
                    t_SalesReturn.SalesReturnWhsName = param.whsName;
                    t_SalesReturn.SalesReturnNotes = param.notes;
                    t_SalesReturn.BPLId = param.branchCode;
                    t_SalesReturn.CreatedBy = param.userId;
                    t_SalesReturn.CreatedOn = dt;
                    t_SalesReturn.ModifiedBy = param.userId;
                    t_SalesReturn.ModifiedOn = dt;
                    db.T_SalesReturn.Add(t_SalesReturn);
                    db.SaveChanges();
                    List<SalesReturnDetailClass> PSJDD = JsonConvert.DeserializeObject<List<SalesReturnDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_SalesReturnDetail t_SalesReturnDetail = new T_SalesReturnDetail();
                        t_SalesReturnDetail.SalesReturnCode = t_SalesReturn.SalesReturnCode;
                        t_SalesReturnDetail.SalesReturnDetailItemCode = datasetdetail.itemCode;
                        t_SalesReturnDetail.SalesReturnDetailItemName = datasetdetail.itemName;
                        t_SalesReturnDetail.SalesReturnDetailItemBarcode = datasetdetail.itemBarcode;
                        t_SalesReturnDetail.SalesReturnDetailItemQty = datasetdetail.itemQty;
                        t_SalesReturnDetail.SalesReturnDetailItemItemType = datasetdetail.itemType;
                        t_SalesReturnDetail.SalesReturnDetailItemCustomerCode = datasetdetail.customerCode;
                        t_SalesReturnDetail.SalesReturnDetailItemCustomerName = datasetdetail.customerName;
                        t_SalesReturnDetail.SalesReturnDetailItemWhsCode = datasetdetail.whsCode;
                        t_SalesReturnDetail.SalesReturnDetailItemWhsName = datasetdetail.whsName;
                        t_SalesReturnDetail.SalesReturnDetailItemUoMCode = datasetdetail.itemUoMCode;
                        t_SalesReturnDetail.DODocNum = datasetdetail.docNum;
                        t_SalesReturnDetail.DODocEntry = datasetdetail.docEntry;
                        t_SalesReturnDetail.DOLineNum = datasetdetail.lineNum;
                        t_SalesReturnDetail.CreatedBy = param.userId;
                        t_SalesReturnDetail.CreatedOn = dt;
                        t_SalesReturnDetail.ModifiedBy = param.userId;
                        t_SalesReturnDetail.ModifiedOn = dt;
                        db.T_SalesReturnDetail.Add(t_SalesReturnDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_SalesReturn.SalesReturnCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.salesReturnId = t_SalesReturn.SalesReturnId;
                    output.salesReturnCode = t_SalesReturn.SalesReturnCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.salesReturnId = 0;
                output.salesReturnCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetSalesReturnResponse getSalesReturn(string getSalesReturn, GetSalesReturnRequest param)
        {
            GetSalesReturnResponse output = new GetSalesReturnResponse();
            try
            {
                output.headers = new List<GetSalesReturnClass>();
                output.details = new List<GetSalesReturnDetailClass>();
                var getSalesReturnHeader = (from th in db.T_SalesReturn.Where(p => p.SalesReturnWhsCode == param.whsCode && p.SyncDate == null)
                                            from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                            from em in db.T_Log.Where(p => p.LogDocId == th.SalesReturnId && p.LogDocCode == th.SalesReturnCode
                                            && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                            group new { th, td } by new
                                              {
                                                  th.SalesReturnCode,
                                                  th.SalesReturnPostingDate,
                                                  th.SalesReturnDocumentDate,
                                                  th.SalesReturnDueDate,
                                                  th.SalesReturnCustomerCode,
                                                  th.SalesReturnCustomerName,
                                                  td.UserCode,
                                                  td.UserName,
                                                  th.SalesReturnNotes,
                                                  th.CreatedOn,
                                                  em.LogMessage
                                              }
                                             into g
                                              select new
                                              {
                                                  g.Key.SalesReturnCode,
                                                  g.Key.SalesReturnPostingDate,
                                                  g.Key.SalesReturnDocumentDate,
                                                  g.Key.SalesReturnDueDate,
                                                  g.Key.SalesReturnCustomerCode,
                                                  g.Key.SalesReturnCustomerName,
                                                  g.Key.UserCode,
                                                  g.Key.UserName,
                                                  g.Key.SalesReturnNotes,
                                                  g.Key.CreatedOn,
                                                  g.Key.LogMessage
                                              }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getSalesReturnHeader.Count; i++)
                {
                    string code = getSalesReturnHeader[i].SalesReturnCode;
                    GetSalesReturnClass getSalesReturnClass = new GetSalesReturnClass();
                    getSalesReturnClass.salesReturnCode = getSalesReturnHeader[i].SalesReturnCode;
                    getSalesReturnClass.salesReturnPostingDate = getSalesReturnHeader[i].SalesReturnPostingDate?.ToString("dd-MMM-yyyy");
                    getSalesReturnClass.salesReturnDocumentDate = getSalesReturnHeader[i].SalesReturnDocumentDate?.ToString("dd-MMM-yyyy");
                    getSalesReturnClass.salesReturnDueDate = getSalesReturnHeader[i].SalesReturnDueDate?.ToString("dd-MMM-yyyy");
                    getSalesReturnClass.salesReturnCustomerCode = getSalesReturnHeader[i].SalesReturnCustomerCode;
                    getSalesReturnClass.salesReturnCustomerName = getSalesReturnHeader[i].SalesReturnCustomerName;
                    getSalesReturnClass.salesReturnUserCode = getSalesReturnHeader[i].UserCode;
                    getSalesReturnClass.salesReturnUserName = getSalesReturnHeader[i].UserName;
                    getSalesReturnClass.salesReturnNotes = getSalesReturnHeader[i].SalesReturnNotes;
                    getSalesReturnClass.errorMessage = getSalesReturnHeader[i].LogMessage;
                    output.headers.Add(getSalesReturnClass);
                    var getDeliveryOrderDetails = db.T_SalesReturnDetail.Where(p => p.SalesReturnCode == code).ToList();
                    for (int j = 0; j < getDeliveryOrderDetails.Count; j++)
                    {
                        GetSalesReturnDetailClass getSalesReturnDetailClass = new GetSalesReturnDetailClass();
                        getSalesReturnDetailClass.salesReturnDetailId = getDeliveryOrderDetails[j].SalesReturnDetailId;
                        getSalesReturnDetailClass.salesReturnDetailitemCode = getDeliveryOrderDetails[j].SalesReturnDetailItemCode;
                        getSalesReturnDetailClass.salesReturnDetailitemName = getDeliveryOrderDetails[j].SalesReturnDetailItemName;
                        getSalesReturnDetailClass.salesReturnDetailitemBarcode = getDeliveryOrderDetails[j].SalesReturnDetailItemBarcode;
                        getSalesReturnDetailClass.salesReturnDetailitemType = getDeliveryOrderDetails[j].SalesReturnDetailItemItemType;
                        getSalesReturnDetailClass.salesReturnDetailitemQty = getDeliveryOrderDetails[j].SalesReturnDetailItemQty;
                        getSalesReturnDetailClass.salesReturnDetailitemPrice = 0;
                        getSalesReturnDetailClass.salesReturnCode = getDeliveryOrderDetails[j].SalesReturnCode;
                        getSalesReturnDetailClass.DOCode = getDeliveryOrderDetails[j].DODocNum;
                        getSalesReturnDetailClass.salesReturnDetailitemOpenQty = 0;
                        getSalesReturnDetailClass.salesReturnDetailItemUoMCode = getDeliveryOrderDetails[j].SalesReturnDetailItemUoMCode;
                        getSalesReturnDetailClass.salesReturnDetailCustomerCode = getDeliveryOrderDetails[j].SalesReturnDetailItemCustomerCode;
                        getSalesReturnDetailClass.salesReturnDetailCustomerName = getDeliveryOrderDetails[j].SalesReturnDetailItemCustomerName;
                        getSalesReturnDetailClass.salesReturnDetailWhsCode = getDeliveryOrderDetails[j].SalesReturnDetailItemWhsCode;
                        getSalesReturnDetailClass.salesReturnDetailWhsName = getDeliveryOrderDetails[j].SalesReturnDetailItemWhsName;
                        getSalesReturnDetailClass.docEntry = getDeliveryOrderDetails[j].DODocEntry;
                        getSalesReturnDetailClass.docNum = getDeliveryOrderDetails[j].DODocNum;
                        output.details.Add(getSalesReturnDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
