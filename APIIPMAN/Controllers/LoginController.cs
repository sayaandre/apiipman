﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using EnDecDll;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class LoginController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public LoginResponse login(string login, LoginRequest param)
        {
            LoginResponse output = new LoginResponse();
            string encryptpass = ModuleEn.AES_Encrypt(ModuleEn.DataEncrypt(param.userPassword));
            var checkLogin = db.M_User.Where(p => p.UserCode == param.userCode && p.UserPassword == encryptpass && p.isActive == true).FirstOrDefault();
            if (checkLogin != null)
            {
                output.result = "OK";
                output.message = "Success";
                output.userId = checkLogin.UserId;
                output.userCode = checkLogin.UserCode;
                output.userName = checkLogin.UserName;
                output.userWhsCode = checkLogin.UserWhsCode;
                output.userWhsName = checkLogin.UserWhsName;
            }
            else
            {
                output.result = "NG";
                output.message = "Wrong Username or Password " + encryptpass;
                output.userId = 0;
                output.userCode = "";
                output.userName = "";
                output.userWhsCode = "";
                output.userWhsName = "";
            }
            return output;
        }

        [HttpPost]
        public GetConfigResponse getConfig(string getConfig, GetConfigRequest param)
        {
            GetConfigResponse output = new GetConfigResponse();
            try
            {
                var checkConfig = db.M_Config.Where(p => p.ConfigName == param.param).FirstOrDefault();
                if (checkConfig != null)
                {
                    output.result = "OK";
                    output.message = "Success";
                    output.isBranch = checkConfig.isBranch == true ? 1 : 0;
                }
                else
                {
                    output.result = "NG";
                    output.message = "Failed";
                }
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }

        [HttpPost]
        public GetBranchResponse getBranch(string getBranch, GetBranchRequest param)
        {
            GetBranchResponse output = new GetBranchResponse();
            output.locations = new List<GetBranchClass>();
            try
            {
                var checkLocations = db.M_ConfigBranch.Where(p => p.UserId == param.userId).ToList();
                for (int i = 0; i < checkLocations.Count; i++)
                {
                    GetBranchClass getBranchClass = new GetBranchClass();
                    getBranchClass.locationCode = checkLocations[i].BranchId;
                    getBranchClass.locationName = checkLocations[i].BranchName;
                    output.locations.Add(getBranchClass);
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }

        [HttpPost]
        public GetWarehouseResponse getWarehouse(string getWarehouse, GetWarehouseRequest param)
        {
            GetWarehouseResponse output = new GetWarehouseResponse();
            output.warehouses = new List<GetWarehouseClass>();
            try
            {
                var checkWarehouse = db.M_ConfigWarehouse.Where(p => p.UserId == param.userId).ToList();
                for (int i = 0; i < checkWarehouse.Count; i++)
                {
                    GetWarehouseClass getWarehouseClass = new GetWarehouseClass();
                    getWarehouseClass.warehouseCode = checkWarehouse[i].WarehouseCode;
                    getWarehouseClass.warehouseName = checkWarehouse[i].WarehouseName;
                    getWarehouseClass.branchId = checkWarehouse[i].BranchId;
                    output.warehouses.Add(getWarehouseClass);
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }

        [HttpPost]
        public GetModuleResponse getModule(string getModule, GetModuleRequest param)
        {
            GetModuleResponse output = new GetModuleResponse();
            output.modules = new List<GetModuleClass>();
            try
            {
                var checkModule = db.M_ConfigModule.Where(p => p.UserId == param.userId).ToList();
                for (int i = 0; i < checkModule.Count; i++)
                {
                    GetModuleClass getModuleClass = new GetModuleClass();
                    getModuleClass.moduleId = checkModule[i].ModuleId;
                    getModuleClass.moduleName = checkModule[i].ModuleName;
                    output.modules.Add(getModuleClass);
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception e)
            {
                output.result = "NG";
                output.message = "Failed";
            }
            return output;
        }
    }
}
