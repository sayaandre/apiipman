﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class GoodsIssueController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitGoodsIssueResponse submitGoodsIssue(string submitGoodsIssue, SubmitGoodsIssueRequest param)
        {
            SubmitGoodsIssueResponse output = new SubmitGoodsIssueResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_GoodsIssue.OrderByDescending(p => p.GoodsIssueId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.GoodsIssueCode.Split('-');
                    storyCode = "GRI-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "GRI-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_GoodsIssue t_GoodsIssue = new T_GoodsIssue();
                    t_GoodsIssue.GoodsIssueCode = storyCode;
                    t_GoodsIssue.GoodsIssuePostingDate = dtPostingDate;
                    t_GoodsIssue.GoodsIssueDocumentDate = dtDocumentDate;
                    t_GoodsIssue.GoodsIssueWhsCode = param.whsCode;
                    t_GoodsIssue.GoodsIssueWhsName = param.whsName;
                    t_GoodsIssue.GoodsIssueNotes = param.notes;
                    t_GoodsIssue.GoodsIssueTypeId = param.typeId;
                    t_GoodsIssue.GoodsIssueTypeName = param.typeName;
                    t_GoodsIssue.BPLId = param.branchCode;
                    t_GoodsIssue.CreatedBy = param.userId;
                    t_GoodsIssue.CreatedOn = dt;
                    t_GoodsIssue.ModifiedBy = param.userId;
                    t_GoodsIssue.ModifiedOn = dt;
                    db.T_GoodsIssue.Add(t_GoodsIssue);
                    db.SaveChanges();
                    List<GoodsIssueDetailClass> PSJDD = JsonConvert.DeserializeObject<List<GoodsIssueDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_GoodsIssueDetail t_GoodsIssueDetail = new T_GoodsIssueDetail();
                        t_GoodsIssueDetail.GoodsIssueCode = t_GoodsIssue.GoodsIssueCode;
                        t_GoodsIssueDetail.GoodsIssueDetailItemCode = datasetdetail.itemCode;
                        t_GoodsIssueDetail.GoodsIssueDetailItemName = datasetdetail.itemName;
                        t_GoodsIssueDetail.GoodsIssueDetailItemBarcode = datasetdetail.itemBarcode;
                        t_GoodsIssueDetail.GoodsIssueDetailItemQty = datasetdetail.itemQty;
                        t_GoodsIssueDetail.GoodsIssueDetailItemType = datasetdetail.itemType;
                        t_GoodsIssueDetail.GoodsIssueDetailItemUomCode = datasetdetail.itemUomCode;
                        t_GoodsIssueDetail.whsCode = datasetdetail.whsCode;
                        t_GoodsIssueDetail.whsName = datasetdetail.whsName;
                        t_GoodsIssueDetail.CreatedBy = param.userId;
                        t_GoodsIssueDetail.CreatedOn = dt;
                        t_GoodsIssueDetail.ModifiedBy = param.userId;
                        t_GoodsIssueDetail.ModifiedOn = dt;
                        db.T_GoodsIssueDetail.Add(t_GoodsIssueDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_GoodsIssue.GoodsIssueCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.goodsIssueId = t_GoodsIssue.GoodsIssueId;
                    output.goodsIssueCode = t_GoodsIssue.GoodsIssueCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.goodsIssueId = 0;
                output.goodsIssueCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetGoodsIssueResponse getGoodsIssue(string getGoodsIssue, GetGoodsIssueRequest param)
        {
            GetGoodsIssueResponse output = new GetGoodsIssueResponse();
            try
            {
                output.headers = new List<GetGoodsIssueClass>();
                output.details = new List<GetGoodsIssueDetailClass>();
                var getGoodsIssueHeader = (from th in db.T_GoodsIssue.Where(p => p.GoodsIssueWhsCode == param.whsCode && p.SyncDate == null)
                                           from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                           from em in db.T_Log.Where(p => p.LogDocId == th.GoodsIssueId && p.LogDocCode == th.GoodsIssueCode 
                                           && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                           group new { th, td } by new
                                             {
                                                 th.GoodsIssueCode,
                                                 th.GoodsIssuePostingDate,
                                                 th.GoodsIssueDocumentDate,
                                                 th.GoodsIssueTypeId,
                                                 th.GoodsIssueTypeName,
                                                 td.UserCode,
                                                 td.UserName,
                                                 th.GoodsIssueWhsCode,
                                                 th.GoodsIssueWhsName,
                                                 th.GoodsIssueNotes,
                                                 th.CreatedOn,
                                                 em.LogMessage
                                             }
                                             into g
                                             select new
                                             {
                                                 g.Key.GoodsIssueCode,
                                                 g.Key.GoodsIssuePostingDate,
                                                 g.Key.GoodsIssueDocumentDate,
                                                 g.Key.GoodsIssueTypeId,
                                                 g.Key.GoodsIssueTypeName,
                                                 g.Key.UserCode,
                                                 g.Key.UserName,
                                                 g.Key.GoodsIssueWhsCode,
                                                 g.Key.GoodsIssueWhsName,
                                                 g.Key.GoodsIssueNotes,
                                                 g.Key.CreatedOn,
                                                 g.Key.LogMessage
                                             }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getGoodsIssueHeader.Count; i++)
                {
                    string code = getGoodsIssueHeader[i].GoodsIssueCode;
                    GetGoodsIssueClass getGoodsIssueClass = new GetGoodsIssueClass();
                    getGoodsIssueClass.goodsIssueCode = getGoodsIssueHeader[i].GoodsIssueCode;
                    getGoodsIssueClass.goodsIssuePostingDate = getGoodsIssueHeader[i].GoodsIssuePostingDate?.ToString("dd-MMM-yyyy");
                    getGoodsIssueClass.goodsIssueDocumentDate = getGoodsIssueHeader[i].GoodsIssueDocumentDate?.ToString("dd-MMM-yyyy");
                    getGoodsIssueClass.goodsIssueType = getGoodsIssueHeader[i].GoodsIssueTypeName;
                    getGoodsIssueClass.goodsIssueUserCode = getGoodsIssueHeader[i].UserCode;
                    getGoodsIssueClass.goodsIssueUserName = getGoodsIssueHeader[i].UserName;
                    getGoodsIssueClass.goodsIssueLocationCode = getGoodsIssueHeader[i].GoodsIssueWhsCode;
                    getGoodsIssueClass.goodsIssueLocationName = getGoodsIssueHeader[i].GoodsIssueWhsName;
                    getGoodsIssueClass.goodsIssueNotes = getGoodsIssueHeader[i].GoodsIssueNotes;
                    getGoodsIssueClass.errorMessage = getGoodsIssueHeader[i].LogMessage;
                    output.headers.Add(getGoodsIssueClass);
                    var getGoodsIssueDetails = db.T_GoodsIssueDetail.Where(p => p.GoodsIssueCode == code).ToList();
                    for (int j = 0; j < getGoodsIssueDetails.Count; j++)
                    {
                        GetGoodsIssueDetailClass getGoodsReceiptDetailClass = new GetGoodsIssueDetailClass();
                        getGoodsReceiptDetailClass.GoodsIssueDetailId = getGoodsIssueDetails[j].GoodsIssueDetailId;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemCode = getGoodsIssueDetails[j].GoodsIssueDetailItemCode;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemName = getGoodsIssueDetails[j].GoodsIssueDetailItemName;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemBarcode = getGoodsIssueDetails[j].GoodsIssueDetailItemBarcode;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemType = getGoodsIssueDetails[j].GoodsIssueDetailItemType;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemUomCode = getGoodsIssueDetails[j].GoodsIssueDetailItemUomCode;
                        getGoodsReceiptDetailClass.GoodsIssueDetailitemQty = getGoodsIssueDetails[j].GoodsIssueDetailItemQty;
                        getGoodsReceiptDetailClass.GoodsIssueCode = code;
                        output.details.Add(getGoodsReceiptDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
