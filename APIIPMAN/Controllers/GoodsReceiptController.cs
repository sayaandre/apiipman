﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class GoodsReceiptController : ApiController
    {
        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitGoodsReceiptResponse submitGoodsReceipt(string submitGoodsReceipt, SubmitGoodsReceiptRequest param)
        {
            SubmitGoodsReceiptResponse output = new SubmitGoodsReceiptResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_GoodsReceipt.OrderByDescending(p => p.GoodsReceiptId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.GoodsReceiptCode.Split('-');
                    storyCode = "GRE-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "GRE-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_GoodsReceipt t_GoodsReceipt = new T_GoodsReceipt();
                    t_GoodsReceipt.GoodsReceiptCode = storyCode;
                    t_GoodsReceipt.GoodsReceiptPostingDate = dtPostingDate;
                    t_GoodsReceipt.GoodsReceiptDocumentDate = dtDocumentDate;
                    t_GoodsReceipt.GoodsReceiptWhsCode = param.whsCode;
                    t_GoodsReceipt.GoodsReceiptWhsName = param.whsName;
                    t_GoodsReceipt.GoodsReceiptNotes = param.notes;
                    t_GoodsReceipt.GoodsReceiptTypeId = param.typeId;
                    t_GoodsReceipt.GoodsReceiptTypeName = param.typeName;
                    t_GoodsReceipt.BPLId = param.branchCode;
                    t_GoodsReceipt.CreatedBy = param.userId;
                    t_GoodsReceipt.CreatedOn = dt;
                    t_GoodsReceipt.ModifiedBy = param.userId;
                    t_GoodsReceipt.ModifiedOn = dt;
                    db.T_GoodsReceipt.Add(t_GoodsReceipt);
                    db.SaveChanges();
                    List<GoodsReceiptDetailClass> PSJDD = JsonConvert.DeserializeObject<List<GoodsReceiptDetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_GoodsReceiptDetail t_GoodsReceiptDetail = new T_GoodsReceiptDetail();
                        t_GoodsReceiptDetail.GoodsReceiptCode = t_GoodsReceipt.GoodsReceiptCode;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemCode = datasetdetail.itemCode;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemName = datasetdetail.itemName;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemBarcode = datasetdetail.itemBarcode;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemQty = datasetdetail.itemQty;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemType = datasetdetail.itemType;
                        t_GoodsReceiptDetail.GoodsReceiptDetailItemUomCode = datasetdetail.itemUomCode;
                        t_GoodsReceiptDetail.whsCode = datasetdetail.whsCode;
                        t_GoodsReceiptDetail.whsName = datasetdetail.whsName;
                        t_GoodsReceiptDetail.CreatedBy = param.userId;
                        t_GoodsReceiptDetail.CreatedOn = dt;
                        t_GoodsReceiptDetail.ModifiedBy = param.userId;
                        t_GoodsReceiptDetail.ModifiedOn = dt;
                        db.T_GoodsReceiptDetail.Add(t_GoodsReceiptDetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_GoodsReceipt.GoodsReceiptCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.goodsReceiptId = t_GoodsReceipt.GoodsReceiptId;
                    output.goodsReceiptCode = t_GoodsReceipt.GoodsReceiptCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.goodsReceiptId = 0;
                output.goodsReceiptCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetGoodsReceiptResponse getGoodsReceipt(string getGoodsReceipt, GetGoodsReceiptRequest param)
        {
            GetGoodsReceiptResponse output = new GetGoodsReceiptResponse();
            try
            {
                output.headers = new List<GetGoodsReceiptClass>();
                output.details = new List<GetGoodsReceiptDetailClass>();
                var getGoodsReceiptHeader = (from th in db.T_GoodsReceipt.Where(p => p.GoodsReceiptWhsCode == param.whsCode && p.SyncDate == null)
                            from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                            from em in db.T_Log.Where(p => p.LogDocId == th.GoodsReceiptId && p.LogDocCode == th.GoodsReceiptCode
                            && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                            group new { th, td } by new
                          {
                            th.GoodsReceiptCode,
                            th.GoodsReceiptPostingDate,
                            th.GoodsReceiptDocumentDate,
                            th.GoodsReceiptTypeId,
                            th.GoodsReceiptTypeName,
                            td.UserCode,
                            td.UserName,
                            th.GoodsReceiptWhsCode,
                            th.GoodsReceiptWhsName,
                            th.GoodsReceiptNotes,
                            th.CreatedOn,
                            em.LogMessage
                          }
                             into g
                          select new
                          {
                              g.Key.GoodsReceiptCode,
                              g.Key.GoodsReceiptPostingDate,
                              g.Key.GoodsReceiptDocumentDate,
                              g.Key.GoodsReceiptTypeId,
                              g.Key.GoodsReceiptTypeName,
                              g.Key.UserCode,
                              g.Key.UserName,
                              g.Key.GoodsReceiptWhsCode,
                              g.Key.GoodsReceiptWhsName,
                              g.Key.GoodsReceiptNotes,
                              g.Key.CreatedOn,
                              g.Key.LogMessage
                          }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getGoodsReceiptHeader.Count; i++)
                {
                    string code = getGoodsReceiptHeader[i].GoodsReceiptCode;
                    GetGoodsReceiptClass getGoodsReceiptClass = new GetGoodsReceiptClass();
                    getGoodsReceiptClass.goodsReceiptCode = getGoodsReceiptHeader[i].GoodsReceiptCode;
                    getGoodsReceiptClass.goodsReceiptPostingDate = getGoodsReceiptHeader[i].GoodsReceiptPostingDate?.ToString("dd-MMM-yyyy");
                    getGoodsReceiptClass.goodsReceiptDocumentDate = getGoodsReceiptHeader[i].GoodsReceiptDocumentDate?.ToString("dd-MMM-yyyy");
                    getGoodsReceiptClass.goodsReceiptType = getGoodsReceiptHeader[i].GoodsReceiptTypeName;
                    getGoodsReceiptClass.goodsReceiptUserCode = getGoodsReceiptHeader[i].UserCode;
                    getGoodsReceiptClass.goodsReceiptUserName = getGoodsReceiptHeader[i].UserName;
                    getGoodsReceiptClass.goodsReceiptLocationCode = getGoodsReceiptHeader[i].GoodsReceiptWhsCode;
                    getGoodsReceiptClass.goodsReceiptLocationName = getGoodsReceiptHeader[i].GoodsReceiptWhsName;
                    getGoodsReceiptClass.goodsReceiptNotes = getGoodsReceiptHeader[i].GoodsReceiptNotes;
                    getGoodsReceiptClass.errorMessage = getGoodsReceiptHeader[i].LogMessage;
                    output.headers.Add(getGoodsReceiptClass);
                    var getGoodsReceiptDetail = db.T_GoodsReceiptDetail.Where(p => p.GoodsReceiptCode == code).ToList();
                    for (int j = 0; j < getGoodsReceiptDetail.Count; j++)
                    {
                        GetGoodsReceiptDetailClass getGoodsReceiptDetailClass = new GetGoodsReceiptDetailClass();
                        getGoodsReceiptDetailClass.GoodsReceiptDetailId = getGoodsReceiptDetail[j].GoodsReceiptDetailId;
                        getGoodsReceiptDetailClass.GoodsReceiptDetailitemCode = getGoodsReceiptDetail[j].GoodsReceiptDetailItemCode;
                        getGoodsReceiptDetailClass.GoodsReceiptDetailitemName = getGoodsReceiptDetail[j].GoodsReceiptDetailItemName;
                        getGoodsReceiptDetailClass.GoodsReceiptDetailitemBarcode = getGoodsReceiptDetail[j].GoodsReceiptDetailItemBarcode;
                        getGoodsReceiptDetailClass.GoodsReceiptDetailitemType = getGoodsReceiptDetail[j].GoodsReceiptDetailItemType;
                        getGoodsReceiptDetailClass.GoodsReceiptDetailitemUomCode = getGoodsReceiptDetail[j].GoodsReceiptDetailItemUomCode;
                        getGoodsReceiptDetailClass.GoodsReceipteDetailitemQty = getGoodsReceiptDetail[j].GoodsReceiptDetailItemQty;
                        getGoodsReceiptDetailClass.GoodsReceiptCode = code;
                        output.details.Add(getGoodsReceiptDetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
