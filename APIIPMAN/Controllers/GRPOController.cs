﻿using APIIPMAN.Models;
using APIIPMAN.Models.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace APIIPMAN.Controllers
{
    public class GRPOController : ApiController
    {

        V_IPMANEntities db = new V_IPMANEntities();

        [HttpPost]
        public SubmitGRPOResponse submitGRPO(string submitGRPO, SubmitGRPORequest param)
        {
            SubmitGRPOResponse output = new SubmitGRPOResponse();
            try
            {
                var dtPostingDate = DateTime.ParseExact(param.postingDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDocumentDate = DateTime.ParseExact(param.documentDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                var dtDueDate = DateTime.ParseExact(param.dueDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                DateTime dt = DateTime.Now;
                string storyCode = "";
                var checkLastStory = db.T_GRPO.OrderByDescending(p => p.GRPOId).FirstOrDefault();
                if (checkLastStory != null)
                {
                    string[] splitfaktur = checkLastStory.GRPOCode.Split('-');
                    storyCode = "GRO-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitfaktur[2]) + 1).ToString().PadLeft(5, '0');
                }
                else
                {
                    storyCode = "GRO-" + dt.ToString("yyMMdd") + "-00001";
                }
                var txOptions = new TransactionOptions();
                txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
                {
                    T_GRPO t_GRPO = new T_GRPO();
                    t_GRPO.GRPOCode = storyCode;
                    t_GRPO.GRPOPostingDate = dtPostingDate;
                    t_GRPO.GRPODocumentDate = dtDocumentDate;
                    t_GRPO.GRPODueDate = dtDueDate;
                    t_GRPO.GRPOVendorCode = param.vendorCode;
                    t_GRPO.GRPOVendorName = param.vendorName;
                    t_GRPO.GRPOWhsCode = param.whsCode;
                    t_GRPO.GRPOWhsName = param.whsName;
                    t_GRPO.GRPONotes = param.notes;
                    t_GRPO.BPLId = param.branchCode;
                    t_GRPO.CreatedBy = param.userId;
                    t_GRPO.CreatedOn = dt;
                    t_GRPO.ModifiedBy = param.userId;
                    t_GRPO.ModifiedOn = dt;
                    db.T_GRPO.Add(t_GRPO);
                    db.SaveChanges();
                    List<GRPODetailClass> PSJDD = JsonConvert.DeserializeObject<List<GRPODetailClass>>(param.details);
                    foreach (var datasetdetail in PSJDD)
                    {
                        T_GRPODetail t_GRPODetail = new T_GRPODetail();
                        t_GRPODetail.GRPOCode = t_GRPO.GRPOCode;
                        t_GRPODetail.GRPODetailItemCode = datasetdetail.itemCode;
                        t_GRPODetail.GRPODetailItemName = datasetdetail.itemName;
                        t_GRPODetail.GRPODetailItemBarcode = datasetdetail.itemBarcode;
                        t_GRPODetail.GRPODetailItemQty = datasetdetail.itemQty;
                        t_GRPODetail.GRPODetailItemPrice = datasetdetail.itemPrice;
                        t_GRPODetail.GRPODetailItemType = datasetdetail.itemType;
                        t_GRPODetail.GRPODetailItemVendorCode = datasetdetail.vendorCode;
                        t_GRPODetail.GRPODetailItemVendorName = datasetdetail.vendorName;
                        t_GRPODetail.GRPODetailItemWhsCode = datasetdetail.whsCode;
                        t_GRPODetail.GRPODetailItemWhsName = datasetdetail.whsName;
                        t_GRPODetail.GRPODetailItemUoMCode = datasetdetail.itemUoMCode;
                        t_GRPODetail.PODocNum = datasetdetail.docNum;
                        t_GRPODetail.PODocEntry = datasetdetail.docEntry;
                        t_GRPODetail.POLineNum = datasetdetail.lineNum;
                        t_GRPODetail.CreatedBy = param.userId;
                        t_GRPODetail.CreatedOn = dt;
                        t_GRPODetail.ModifiedBy = param.userId;
                        t_GRPODetail.ModifiedOn = dt;
                        db.T_GRPODetail.Add(t_GRPODetail);
                        db.SaveChanges();
                    }
                    List<DocumentBatchClass> PSJDDD = JsonConvert.DeserializeObject<List<DocumentBatchClass>>(param.batch);
                    foreach (var datasetdetail in PSJDDD)
                    {
                        T_DocumentItemDetail t_DocumentItemDetail = new T_DocumentItemDetail();
                        t_DocumentItemDetail.DocumentItemDetailNo = datasetdetail.DocumentItemDetailNo;
                        t_DocumentItemDetail.DocumentItemDetailQty = datasetdetail.DocumentItemDetailQty;
                        t_DocumentItemDetail.DocumentItemDetailUoMCode = datasetdetail.DocumentItemDetailUoMCode;
                        t_DocumentItemDetail.DocumentItemDetailTypeName = datasetdetail.DocumentItemDetailTypeName;
                        t_DocumentItemDetail.DocumentItemDetailItemCode = datasetdetail.DocumentItemDetailItemCode;
                        t_DocumentItemDetail.DocumentItemDetailDocumentName = datasetdetail.DocumentItemDetailDocumentName;
                        t_DocumentItemDetail.DocumentItemDetailHeaderCode = t_GRPO.GRPOCode;
                        t_DocumentItemDetail.CreatedBy = param.userId;
                        t_DocumentItemDetail.CreatedOn = dt;
                        t_DocumentItemDetail.ModifiedBy = param.userId;
                        t_DocumentItemDetail.ModifiedOn = dt;
                        db.T_DocumentItemDetail.Add(t_DocumentItemDetail);
                        db.SaveChanges();
                    }
                    transaction.Complete();
                    output.result = "OK";
                    output.message = "Success";
                    output.grpoId = t_GRPO.GRPOId;
                    output.grpoCode = t_GRPO.GRPOCode;
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
                output.grpoId = 0;
                output.grpoCode = "";
            }
            return output;
        }

        [HttpPost]
        public GetGRPOResponse getGRPO(string getGRPO, GetGRPORequest param)
        {
            GetGRPOResponse output = new GetGRPOResponse();
            try
            {
                output.headers = new List<GetGRPOClass>();
                output.details = new List<GetGRPODetailClass>();
                var getGRPOHeader = (from th in db.T_GRPO.Where(p => p.GRPOWhsCode == param.whsCode && p.SyncDate == null)
                                     from td in db.M_User.Where(p => p.UserId == th.CreatedBy)
                                     from em in db.T_Log.Where(p => p.LogDocId == th.GRPOId && p.LogDocCode == th.GRPOCode
                                     && p.LogStatus != "Cancel" && p.LogStatus != "Success").DefaultIfEmpty()
                                     group new { th, td } by new
                                           {
                                               th.GRPOCode,
                                               th.GRPOPostingDate,
                                               th.GRPODocumentDate,
                                               th.GRPODueDate,
                                               th.GRPOVendorCode,
                                               th.GRPOVendorName,
                                               td.UserCode,
                                               td.UserName,
                                               th.GRPONotes,
                                               th.GRPOId,
                                               th.CreatedOn,
                                               em.LogMessage
                                           }
                                             into g
                                           select new
                                           {
                                               g.Key.GRPOCode,
                                               g.Key.GRPOPostingDate,
                                               g.Key.GRPODocumentDate,
                                               g.Key.GRPODueDate,
                                               g.Key.GRPOVendorCode,
                                               g.Key.GRPOVendorName,
                                               g.Key.UserCode,
                                               g.Key.UserName,
                                               g.Key.GRPONotes,
                                               g.Key.GRPOId,
                                               g.Key.CreatedOn,
                                               g.Key.LogMessage
                                           }).OrderByDescending(p => p.CreatedOn).ToList();
                for (int i = 0; i < getGRPOHeader.Count; i++)
                {
                    string code = getGRPOHeader[i].GRPOCode;
                    GetGRPOClass getGRPOClass = new GetGRPOClass();
                    getGRPOClass.GRPOCode = getGRPOHeader[i].GRPOCode;
                    getGRPOClass.GRPOPostingDate = getGRPOHeader[i].GRPOPostingDate?.ToString("dd-MMM-yyyy");
                    getGRPOClass.GRPODocumentDate = getGRPOHeader[i].GRPODocumentDate?.ToString("dd-MMM-yyyy");
                    getGRPOClass.GRPODueDate = getGRPOHeader[i].GRPODueDate?.ToString("dd-MMM-yyyy");
                    getGRPOClass.GRPOVendorCode = getGRPOHeader[i].GRPOVendorCode;
                    getGRPOClass.GRPOVendorName = getGRPOHeader[i].GRPOVendorName;
                    getGRPOClass.GRPOUserCode = getGRPOHeader[i].UserCode;
                    getGRPOClass.GRPOUserName = getGRPOHeader[i].UserName;
                    getGRPOClass.GRPONotes = getGRPOHeader[i].GRPONotes;
                    getGRPOClass.GRPOId = getGRPOHeader[i].GRPOId;
                    getGRPOClass.errorMessage = getGRPOHeader[i].LogMessage;
                    output.headers.Add(getGRPOClass);
                    var getGRPODetails = db.T_GRPODetail.Where(p => p.GRPOCode == code).ToList();
                    for (int j = 0; j < getGRPODetails.Count; j++)
                    {
                        GetGRPODetailClass getGRPODetailClass = new GetGRPODetailClass();
                        getGRPODetailClass.GRPODetailId = getGRPODetails[j].GRPODetailId;
                        getGRPODetailClass.GRPODetailitemCode = getGRPODetails[j].GRPODetailItemCode;
                        getGRPODetailClass.GRPODetailitemName = getGRPODetails[j].GRPODetailItemName;
                        getGRPODetailClass.GRPODetailitemBarcode = getGRPODetails[j].GRPODetailItemBarcode;
                        getGRPODetailClass.GRPODetailitemType = getGRPODetails[j].GRPODetailItemType;
                        getGRPODetailClass.GRPODetailitemQty = getGRPODetails[j].GRPODetailItemQty;
                        getGRPODetailClass.GRPODetailitemPrice = getGRPODetails[j].GRPODetailItemPrice;
                        getGRPODetailClass.GRPOCode = getGRPODetails[j].GRPOCode;
                        getGRPODetailClass.POCode = getGRPODetails[j].PODocNum;
                        getGRPODetailClass.GRPODetailitemOpenQty = 0;
                        getGRPODetailClass.GRPODetailItemUoMCode = getGRPODetails[j].GRPODetailItemUoMCode;
                        getGRPODetailClass.GRPODetailVendorCode = getGRPODetails[j].GRPODetailItemVendorCode;
                        getGRPODetailClass.GRPODetailVendorName = getGRPODetails[j].GRPODetailItemVendorName;
                        getGRPODetailClass.GRPODetailWhsCode = getGRPODetails[j].GRPODetailItemWhsCode;
                        getGRPODetailClass.GRPODetailWhsName = getGRPODetails[j].GRPODetailItemWhsName;
                        getGRPODetailClass.docEntry = getGRPODetails[j].PODocEntry;
                        getGRPODetailClass.docNum = getGRPODetails[j].PODocNum;
                        output.details.Add(getGRPODetailClass);
                    }
                }
                output.result = "OK";
                output.message = "Success";
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.Message;
            }
            return output;
        }
    }
}
