//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_InventoryTransferDetail
    {
        public long InventoryTransferDetailId { get; set; }
        public string InventoryTransferDetailItemCode { get; set; }
        public string InventoryTransferDetailItemName { get; set; }
        public string InventoryTransferDetailItemBarcode { get; set; }
        public Nullable<int> InventoryTransferDetailItemQty { get; set; }
        public string InventoryTransferDetailItemType { get; set; }
        public string InventoryTransferDetailItemUoMCode { get; set; }
        public string InventoryTransferCode { get; set; }
        public Nullable<long> CreatedOn { get; set; }
        public Nullable<System.DateTime> CreatedBy { get; set; }
        public Nullable<long> ModifiedOn { get; set; }
        public Nullable<System.DateTime> ModifiedBy { get; set; }
    }
}
