//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_GoodsIssueDetail
    {
        public long GoodsIssueDetailId { get; set; }
        public string GoodsIssueDetailItemCode { get; set; }
        public string GoodsIssueDetailItemName { get; set; }
        public string GoodsIssueDetailItemBarcode { get; set; }
        public Nullable<int> GoodsIssueDetailItemQty { get; set; }
        public string GoodsIssueDetailItemType { get; set; }
        public string GoodsIssueDetailItemUomCode { get; set; }
        public string GoodsIssueCode { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
