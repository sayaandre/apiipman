//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_SalesReturnDetail
    {
        public long SalesReturnDetailId { get; set; }
        public string SalesReturnDetailItemCode { get; set; }
        public string SalesReturnDetailItemName { get; set; }
        public string SalesReturnDetailItemBarcode { get; set; }
        public string SalesReturnDetailItemUoMCode { get; set; }
        public Nullable<int> SalesReturnDetailItemQty { get; set; }
        public string SalesReturnDetailItemItemType { get; set; }
        public string SalesReturnCode { get; set; }
        public string SalesReturnDetailItemCustomerCode { get; set; }
        public string SalesReturnDetailItemCustomerName { get; set; }
        public string SalesReturnDetailItemWhsCode { get; set; }
        public string SalesReturnDetailItemWhsName { get; set; }
        public string DODocNum { get; set; }
        public Nullable<int> DODocEntry { get; set; }
        public Nullable<int> DOLineNum { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
