//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_GoodsIssue
    {
        public long GoodsIssueId { get; set; }
        public string GoodsIssueCode { get; set; }
        public Nullable<System.DateTime> GoodsIssuePostingDate { get; set; }
        public Nullable<System.DateTime> GoodsIssueDocumentDate { get; set; }
        public Nullable<long> GoodsIssueTypeId { get; set; }
        public string GoodsIssueTypeName { get; set; }
        public string GoodsIssueWhsCode { get; set; }
        public string GoodsIssueWhsName { get; set; }
        public string GoodsIssueNotes { get; set; }
        public Nullable<long> BPLId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string DocNum { get; set; }
        public Nullable<int> DocEntry { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public Nullable<bool> IsSync { get; set; }
        public Nullable<long> CancelBy { get; set; }
        public Nullable<System.DateTime> CancelOn { get; set; }
    }
}
