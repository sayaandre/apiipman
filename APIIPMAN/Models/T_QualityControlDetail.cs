//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_QualityControlDetail
    {
        public long QualityControlDetailId { get; set; }
        public string QualityControlDetailItemCode { get; set; }
        public string QualityControlDetailItemName { get; set; }
        public string QualityControlDetailItemUoMCode { get; set; }
        public Nullable<int> QualityControlDetailItemQty { get; set; }
        public Nullable<int> QualityControlDetailItemQtyQc { get; set; }
        public string QualityControlDetailItemStatusName { get; set; }
        public string QualityControlCode { get; set; }
        public string DocNum { get; set; }
        public Nullable<int> DocEntry { get; set; }
        public Nullable<int> LineNum { get; set; }
    }
}
