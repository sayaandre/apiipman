//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_DeliveryOrder
    {
        public long DeliveryOrderId { get; set; }
        public string DeliveryOrderCode { get; set; }
        public Nullable<System.DateTime> DeliveryOrderPostingDate { get; set; }
        public Nullable<System.DateTime> DeliveryOrderDocumentDate { get; set; }
        public Nullable<System.DateTime> DeliveryOrderDeliveryDate { get; set; }
        public string DeliveryOrderCustomerCode { get; set; }
        public string DeliveryOrderCustomerName { get; set; }
        public string DeliveryOrderWhsCode { get; set; }
        public string DeliveryOrderWhsName { get; set; }
        public string DeliveryOrderNotes { get; set; }
        public Nullable<long> BPLId { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string DocNum { get; set; }
        public Nullable<int> DocEntry { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
        public Nullable<bool> IsSync { get; set; }
        public Nullable<long> CancelBy { get; set; }
        public Nullable<System.DateTime> CancelOn { get; set; }
    }
}
