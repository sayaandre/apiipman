﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{

    public class SubmitGoodsReturnRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string dueDate { get; set; }
        public string vendorCode { get; set; }
        public string vendorName { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitGoodsReturnResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long goodsReturnId { get; set; }
        public string goodsReturnCode { get; set; }
    }

    public class GoodsReturnDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public int grpoDocNum { get; set; }
        public int grpoDocEntry { get; set; }
        public string itemType { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string vendorCode { get; set; }
        public string vendorName { get; set; }
        public string itemUoMCode { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int lineNum { get; set; }
    }

    public class GetGoodsReturnRequest
    {
        public string whsCode { get; set; }
    }

    public class GetGoodsReturnResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetGoodsReturnClass> headers { get; set; }
        public List<GetGoodsReturnDetailClass> details { get; set; }
    }

    public class GetGoodsReturnClass
    {
        public string goodsReturnCode { get; set; }
        public string goodsReturnPostingDate { get; set; }
        public string goodsReturnDocumentDate { get; set; }
        public string goodsReturnDueDate { get; set; }
        public string goodsReturnVendorCode { get; set; }
        public string goodsReturnVendorName { get; set; }
        public string goodsReturnUserCode { get; set; }
        public string goodsReturnUserName { get; set; }
        public string goodsReturnNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetGoodsReturnDetailClass
    {
        public long goodsReturnDetailId { get; set; }
        public string goodsReturnDetailitemCode { get; set; }
        public string goodsReturnDetailitemName { get; set; }
        public string goodsReturnDetailitemBarcode { get; set; }
        public string goodsReturnDetailitemType { get; set; }
        public int? goodsReturnDetailitemQty { get; set; }
        public double? goodsReturnDetailitemPrice { get; set; }
        public string goodsReturnCode { get; set; }
        public string GRPOCode { get; set; }
        public int? goodsReturnDetailitemOpenQty { get; set; }
        public string goodsReturnDetailItemUoMCode { get; set; }
        public string goodsReturnDetailVendorCode { get; set; }
        public string goodsReturnDetailVendorName { get; set; }
        public string goodsReturnDetailWhsCode { get; set; }
        public string goodsReturnDetailWhsName { get; set; }
        public int? docEntry { get; set; }
        public string docNum { get; set; }
    }

    public class GoodsReturnClass
    {
    }
}