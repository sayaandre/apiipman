﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class DocumentBatchClass
    {
        public string DocumentItemDetailNo { get; set; }
        public double DocumentItemDetailQty { get; set; }
        public string DocumentItemDetailUoMCode { get; set; }
        public string DocumentItemDetailTypeName { get; set; }
        public string DocumentItemDetailItemCode { get; set; }
        public string DocumentItemDetailDocumentName { get; set; }
    }
}