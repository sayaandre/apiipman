﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitDeliveryOrderRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitDeliveryOrderResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long deliveryOrderId { get; set; }
        public string deliveryOrderCode { get; set; }
    }

    public class DeliveryOrderDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public int soDocNum { get; set; }
        public int soDocEntry { get; set; }
        public string itemType { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string itemUoMCode { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int lineNum { get; set; }
    }

    public class GetDeliveryOrderRequest
    {
        public string whsCode { get; set; }
    }

    public class GetDeliveryOrderResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetDeliveryOrderClass> headers { get; set; }
        public List<GetDeliveryOrderDetailClass> details { get; set; }
    }

    public class GetDeliveryOrderClass
    {
        public string DeliveryOrderCode { get; set; }
        public string DeliveryOrderPostingDate { get; set; }
        public string DeliveryOrderDocumentDate { get; set; }
        public string DeliveryOrderDeliveryDate { get; set; }
        public string DeliveryOrderCustomerCode { get; set; }
        public string DeliveryOrderCustomerName { get; set; }
        public string DeliveryOrderUserCode { get; set; }
        public string DeliveryOrderUserName { get; set; }
        public string DeliveryOrderNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetDeliveryOrderDetailClass
    {
        public long DeliveryOrderDetailId { get; set; }
        public string DeliveryOrderDetailitemCode { get; set; }
        public string DeliveryOrderDetailitemName { get; set; }
        public string DeliveryOrderDetailitemBarcode { get; set; }
        public string DeliveryOrderDetailitemType { get; set; }
        public int? DeliveryOrderDetailitemQty { get; set; }
        public double? DeliveryOrderDetailitemPrice { get; set; }
        public string DeliveryOrderCode { get; set; }
        public string SOCode { get; set; }
        public int? DeliveryOrderDetailitemOpenQty { get; set; }
        public string DeliveryOrderDetailItemUoMCode { get; set; }
        public string DeliveryOrderDetailCustomerCode { get; set; }
        public string DeliveryOrderDetailCustomerName { get; set; }
        public string DeliveryOrderDetailWhsCode { get; set; }
        public string DeliveryOrderDetailWhsName { get; set; }
        public int? docEntry { get; set; }
        public string docNum { get; set; }
    }

    public class DeliveryOrderClass
    {
    }
}