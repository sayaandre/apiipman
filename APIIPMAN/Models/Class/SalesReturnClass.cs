﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{

    public class SubmitSalesReturnRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitSalesReturnResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long salesReturnId { get; set; }
        public string salesReturnCode { get; set; }
    }

    public class SalesReturnDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public int doDocNum { get; set; }
        public int doDocEntry { get; set; }
        public string itemType { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string itemUoMCode { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int lineNum { get; set; }
    }

    public class GetSalesReturnRequest
    {
        public string whsCode { get; set; }
    }

    public class GetSalesReturnResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetSalesReturnClass> headers { get; set; }
        public List<GetSalesReturnDetailClass> details { get; set; }
    }

    public class GetSalesReturnClass
    {
        public string salesReturnCode { get; set; }
        public string salesReturnPostingDate { get; set; }
        public string salesReturnDocumentDate { get; set; }
        public string salesReturnDueDate { get; set; }
        public string salesReturnCustomerCode { get; set; }
        public string salesReturnCustomerName { get; set; }
        public string salesReturnUserCode { get; set; }
        public string salesReturnUserName { get; set; }
        public string salesReturnNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetSalesReturnDetailClass
    {
        public long salesReturnDetailId { get; set; }
        public string salesReturnDetailitemCode { get; set; }
        public string salesReturnDetailitemName { get; set; }
        public string salesReturnDetailitemBarcode { get; set; }
        public string salesReturnDetailitemType { get; set; }
        public int? salesReturnDetailitemQty { get; set; }
        public double? salesReturnDetailitemPrice { get; set; }
        public string salesReturnCode { get; set; }
        public string DOCode { get; set; }
        public int? salesReturnDetailitemOpenQty { get; set; }
        public string salesReturnDetailItemUoMCode { get; set; }
        public string salesReturnDetailCustomerCode { get; set; }
        public string salesReturnDetailCustomerName { get; set; }
        public string salesReturnDetailWhsCode { get; set; }
        public string salesReturnDetailWhsName { get; set; }
        public int? docEntry { get; set; }
        public string docNum { get; set; }
    }

    public class SalesReturnClass
    {
    }
}