﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitQualityControlRequest
    {
        public string date { get; set; }
        public string type { get; set; }
        public string notes { get; set; }
        public string headerImage { get; set; }
        public string shift { get; set; }
        public string machine { get; set; }
        public string time { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string images { get; set; }
    }

    public class SubmitQualityControlResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public string qcCode { get; set; }
        public long qcId { get; set; }
    }

    public class QualityControlDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string uomCode { get; set; }
        public int qty { get; set; }
        public int qtyQc { get; set; }
        public string itemStatus { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int lineNum { get; set; }
    }

    public class QualityControlDetailImageClass
    {
        public string itemCode { get; set; }
        public int qty { get; set; }
        public string status { get; set; }
        public string image { get; set; }
    }

    public class GetQCDocEntryResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public string docEntry { get; set; }
    }

    public class GetQCDocEntryRequest
    {
        public string param { get; set; }
    }

    public class QualityControlClass
    {
    }
}