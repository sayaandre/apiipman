﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class LoginRequest
    {
        public string userCode { get; set; }
        public string userPassword { get; set; }
    }

    public class LoginResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long userId { get; set; }
        public string userCode { get; set; }
        public string userName { get; set; }
        public string userWhsCode { get; set; }
        public string userWhsName { get; set; }
    }

    public class GetBranchRequest
    {
        public long userId { get; set; }
    }

    public class GetBranchResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetBranchClass> locations { get; set; }
    }

    public class GetBranchClass
    {
        public int? locationCode { get; set; }
        public string locationName { get; set; }
        public string defaultWhs { get; set; }
    }

    public class GetWarehouseRequest
    {
        public long userId { get; set; }
    }

    public class GetWarehouseResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetWarehouseClass> warehouses { get; set; }
    }

    public class GetWarehouseClass
    {
        public string warehouseCode { get; set; }
        public string warehouseName { get; set; }
        public int? branchId { get; set; }
    }

    public class GetModuleRequest
    {
        public long userId { get; set; }
    }

    public class GetModuleResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetModuleClass> modules { get; set; }
    }

    public class GetModuleClass
    {
        public long? moduleId { get; set; }
        public string moduleName { get; set; }
    }

    public class GetConfigRequest
    {
        public string param { get; set; }
    }

    public class GetConfigResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public int isBranch { get; set; }
    }

    public class LoginClass
    {
    }
}