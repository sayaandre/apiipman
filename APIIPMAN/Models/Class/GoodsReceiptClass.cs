﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{

    public class SubmitGoodsReceiptRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public int typeId { get; set; }
        public string typeName { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitGoodsReceiptResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long goodsReceiptId { get; set; }
        public string goodsReceiptCode { get; set; }
    }

    public class GoodsReceiptDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public string itemType { get; set; }
        public string itemUomCode { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
    }

    public class GetGoodsReceiptRequest
    {
        public string whsCode { get; set; }
    }

    public class GetGoodsReceiptResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetGoodsReceiptClass> headers { get; set; }
        public List<GetGoodsReceiptDetailClass> details { get; set; }
    }

    public class GetGoodsReceiptClass
    {
        public string goodsReceiptCode { get; set; }
        public string goodsReceiptPostingDate { get; set; }
        public string goodsReceiptDocumentDate { get; set; }
        public string goodsReceiptType { get; set; }
        public string goodsReceiptUserCode { get; set; }
        public string goodsReceiptUserName { get; set; }
        public string goodsReceiptLocationCode { get; set; }
        public string goodsReceiptLocationName { get; set; }
        public string goodsReceiptNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetGoodsReceiptDetailClass
    {
        public long GoodsReceiptDetailId { get; set; }
        public string GoodsReceiptDetailitemCode { get; set; }
        public string GoodsReceiptDetailitemName { get; set; }
        public string GoodsReceiptDetailitemBarcode { get; set; }
        public string GoodsReceiptDetailitemType { get; set; }
        public string GoodsReceiptDetailitemUomCode { get; set; }
        public int? GoodsReceipteDetailitemQty { get; set; }
        public double GoodsReceiptDetailitemPrice { get; set; }
        public string GoodsReceiptCode { get; set; }
    }

    public class GoodsReceiptClass
    {
    }
}