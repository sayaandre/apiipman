﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitGoodsIssueRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public int typeId { get; set; }
        public string typeName { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitGoodsIssueResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long goodsIssueId { get; set; }
        public string goodsIssueCode { get; set; }
    }

    public class GoodsIssueDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public string itemType { get; set; }
        public string itemUomCode { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
    }

    public class GetGoodsIssueRequest
    {
        public string whsCode { get; set; }
    }

    public class GetGoodsIssueResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetGoodsIssueClass> headers { get; set; }
        public List<GetGoodsIssueDetailClass> details { get; set; }
    }

    public class GetGoodsIssueClass
    {
        public string goodsIssueCode { get; set; }
        public string goodsIssuePostingDate { get; set; }
        public string goodsIssueDocumentDate { get; set; }
        public string goodsIssueType { get; set; }
        public string goodsIssueUserCode { get; set; }
        public string goodsIssueUserName { get; set; }
        public string goodsIssueLocationCode { get; set; }
        public string goodsIssueLocationName { get; set; }
        public string goodsIssueNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetGoodsIssueDetailClass
    {
        public long GoodsIssueDetailId { get; set; }
        public string GoodsIssueDetailitemCode { get; set; }
        public string GoodsIssueDetailitemName { get; set; }
        public string GoodsIssueDetailitemBarcode { get; set; }
        public string GoodsIssueDetailitemType { get; set; }
        public string GoodsIssueDetailitemUomCode { get; set; }
        public int? GoodsIssueDetailitemQty { get; set; }
        public double GoodsIssueDetailitemPrice { get; set; }
        public string GoodsIssueCode { get; set; }
    }
    public class GoodsIssueClass
    {
    }
}