﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitGRPORequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string dueDate { get; set; }
        public string vendorCode { get; set; }
        public string vendorName { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitGRPOResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long grpoId { get; set; }
        public string grpoCode { get; set; }
    }

    public class GRPODetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public int poDocNum { get; set; }
        public int poDocEntry { get; set; }
        public string itemType { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string vendorCode { get; set; }
        public string vendorName { get; set; }
        public string itemUoMCode { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int lineNum { get; set; }
    }

    public class GetGRPORequest
    {
        public string whsCode { get; set; }
    }

    public class GetGRPOResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetGRPOClass> headers { get; set; }
        public List<GetGRPODetailClass> details { get; set; }
    }

    public class GetGRPOClass
    {
        public string GRPOCode { get; set; }
        public string GRPOPostingDate { get; set; }
        public string GRPODocumentDate { get; set; }
        public string GRPODueDate { get; set; }
        public string GRPOVendorCode { get; set; }
        public string GRPOVendorName { get; set; }
        public string GRPOUserCode { get; set; }
        public string GRPOUserName { get; set; }
        public string GRPONotes { get; set; }
        public long GRPOId { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetGRPODetailClass
    {
        public long GRPODetailId { get; set; }
        public string GRPODetailitemCode { get; set; }
        public string GRPODetailitemName { get; set; }
        public string GRPODetailitemBarcode { get; set; }
        public string GRPODetailitemType { get; set; }
        public int? GRPODetailitemQty { get; set; }
        public double? GRPODetailitemPrice { get; set; }
        public string GRPOCode { get; set; }
        public string POCode { get; set; }
        public int? GRPODetailitemOpenQty { get; set; }
        public string GRPODetailItemUoMCode { get; set; }
        public string GRPODetailVendorCode { get; set; }
        public string GRPODetailVendorName { get; set; }
        public string GRPODetailWhsCode { get; set; }
        public string GRPODetailWhsName { get; set; }
        public int? docEntry { get; set; }
        public string docNum { get; set; }
    }

    public class GRPOClass
    {
    }
}