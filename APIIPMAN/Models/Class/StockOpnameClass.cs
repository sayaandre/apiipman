﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitStockOpnameRequest
    {
        public string countingDate { get; set; }
        public string postingDate { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public string notes { get; set; }
        public string docNum { get; set; }
        public int docEntry { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitStockOpnameResponse
    {
        public string result { get; set; }
        public string message { get; set; }
    }

    public class StockOpnameDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public string itemType { get; set; }
        public string itemUomCode { get; set; }
        public string whsCode { get; set; }
        public string whsName { get; set; }
        public int lineNum { get; set; }
    }

    public class GetStockOpnameRequest
    {
        public string whsCode { get; set; }
    }

    public class GetStockOpnameResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetStockOpnameClass> headers { get; set; }
    }

    public class GetStockOpnameClass
    {
        public long stockOpnameId { get; set; }
        public string stockOpnameCode { get; set; }
        public string docNum { get; set; }
        public int? docEntry { get; set; }
        public long logId { get; set; }
        public string logMessage { get; set; }
    }

    public class GetStockOpnameDetailByIdRequest
    {
        public string docNum { get; set; }
    }

    public class GetStockOpnameDetailByIdResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetStockOpnameDetailClass> details { get; set; }
    }

    public class GetStockOpnameDetailClass
    {
        public long stockOpnameDetailId { get; set; }
        public string stockOpnameDetailItemCode { get; set; }
        public string stockOpnameDetailItemName { get; set; }
        public string stockOpnameDetailItemBarcode { get; set; }
        public string stockOpnameDetailWhsCode { get; set; }
        public string stockOpnameDetailWhsName { get; set; }
        public int? stockOpnameDetailCountQty { get; set; }
        public string stockOpnameDetailItemType { get; set; }
        public int? docEntry { get; set; }
        public int? lineNum { get; set; }
    }

    public class StockOpnameClass
    {
    }
}