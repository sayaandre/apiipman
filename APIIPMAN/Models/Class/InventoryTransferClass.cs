﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APIIPMAN.Models.Class
{
    public class SubmitInventoryTransferRequest
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string fromCode { get; set; }
        public string fromName { get; set; }
        public string toCode { get; set; }
        public string toName { get; set; }
        public string type { get; set; }
        public string notes { get; set; }
        public int branchCode { get; set; }
        public long userId { get; set; }
        public string details { get; set; }
        public string batch { get; set; }
    }

    public class SubmitInventoryTransferResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public long inventoryTransferId { get; set; }
        public string inventoryTransferCode { get; set; }
    }

    public class InventoryTransferDetailClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemBarcode { get; set; }
        public int itemQty { get; set; }
        public string itemType { get; set; }
        public string itemUomCode { get; set; }
    }

    public class GetInventoryTransferRequest
    {
        public string whsCode { get; set; }
    }

    public class GetInventoryTransferResponse
    {
        public string result { get; set; }
        public string message { get; set; }
        public List<GetInventoryTransferClass> headers { get; set; }
        public List<GetInventoryTransferDetailClass> details { get; set; }
    }

    public class GetInventoryTransferClass
    {
        public string inventoryTransferCode { get; set; }
        public string inventoryTransferPostingDate { get; set; }
        public string inventoryTransferDocumentDate { get; set; }
        public string inventoryTransferFromCode { get; set; }
        public string inventoryTransferFromName { get; set; }
        public string inventoryTransferToCode { get; set; }
        public string inventoryTransferToName { get; set; }
        public string inventoryTransferNotes { get; set; }
        public string errorMessage { get; set; }
    }

    public class GetInventoryTransferDetailClass
    {
        public long inventoryTransferDetailId { get; set; }
        public string inventoryTransferDetailitemCode { get; set; }
        public string inventoryTransferDetailitemName { get; set; }
        public string inventoryTransferDetailitemBarcode { get; set; }
        public string inventoryTransferDetailitemType { get; set; }
        public int? inventoryTransferDetailitemQty { get; set; }
        public string inventoryTransferDetailitemUomCode { get; set; }
        public double inventoryTransferDetailitemPrice { get; set; }
        public string inventoryTransferCode { get; set; }
    }

    public class InventoryTransferClass
    {
    }
}