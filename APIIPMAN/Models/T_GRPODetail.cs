//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APIIPMAN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_GRPODetail
    {
        public long GRPODetailId { get; set; }
        public string GRPODetailItemCode { get; set; }
        public string GRPODetailItemName { get; set; }
        public string GRPODetailItemBarcode { get; set; }
        public string GRPODetailItemUoMCode { get; set; }
        public Nullable<int> GRPODetailItemQty { get; set; }
        public Nullable<double> GRPODetailItemPrice { get; set; }
        public string GRPODetailItemType { get; set; }
        public string GRPOCode { get; set; }
        public string GRPODetailItemVendorCode { get; set; }
        public string GRPODetailItemVendorName { get; set; }
        public string GRPODetailItemWhsCode { get; set; }
        public string GRPODetailItemWhsName { get; set; }
        public string PODocNum { get; set; }
        public Nullable<int> PODocEntry { get; set; }
        public Nullable<int> POLineNum { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
